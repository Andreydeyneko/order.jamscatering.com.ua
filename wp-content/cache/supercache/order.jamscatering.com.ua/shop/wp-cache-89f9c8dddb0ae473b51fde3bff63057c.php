<?php die(); ?><!DOCTYPE html>
<html lang="uk">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="https://order.jamscatering.com.ua/xmlrpc.php">

<title>Товари &#8211; Jam&#039;s catering</title>
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Jam&#039;s catering &raquo; стрічка" href="https://order.jamscatering.com.ua/feed/" />
<link rel="alternate" type="application/rss+xml" title="Jam&#039;s catering &raquo; Канал коментарів" href="https://order.jamscatering.com.ua/comments/feed/" />
<link rel="alternate" type="text/calendar" title="Jam&#039;s catering &raquo; iCal Feed" href="https://order.jamscatering.com.ua/events/?ical=1" />
<link rel="alternate" type="application/rss+xml" title="Канал Jam&#039;s catering &raquo; Товари" href="https://order.jamscatering.com.ua/shop/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/order.jamscatering.com.ua\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.9"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='dashicons-css'  href='https://order.jamscatering.com.ua/wp-includes/css/dashicons.min.css?ver=4.9.9' type='text/css' media='all' />
<style id='dashicons-inline-css' type='text/css'>
[data-font="Dashicons"]:before {font-family: 'Dashicons' !important;content: attr(data-icon) !important;speak: none !important;font-weight: normal !important;font-variant: normal !important;text-transform: none !important;line-height: 1 !important;font-style: normal !important;-webkit-font-smoothing: antialiased !important;-moz-osx-font-smoothing: grayscale !important;}
</style>
<link rel='stylesheet' id='admin-bar-css'  href='https://order.jamscatering.com.ua/wp-includes/css/admin-bar.min.css?ver=4.9.9' type='text/css' media='all' />
<link rel='stylesheet' id='tribe-events-admin-menu-css'  href='https://order.jamscatering.com.ua/wp-content/plugins/the-events-calendar/src/resources/css/admin-menu.min.css?ver=4.6.26' type='text/css' media='all' />
<link rel='stylesheet' id='pizzaro-woocommerce-style-css'  href='https://order.jamscatering.com.ua/wp-content/themes/pizzaro/assets/css/woocommerce/woocommerce.css?ver=4.9.9' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='https://order.jamscatering.com.ua/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.5' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='https://order.jamscatering.com.ua/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.4.8' type='text/css' media='all' />
<style id='rs-plugin-settings-inline-css' type='text/css'>
#rs-demo-id {}
</style>
<style id='woocommerce-inline-inline-css' type='text/css'>
.woocommerce form .form-row .required { visibility: visible; }
</style>
<link rel='stylesheet' id='wpsl-styles-css'  href='https://order.jamscatering.com.ua/wp-content/plugins/wp-store-locator/css/styles.min.css?ver=2.2.17' type='text/css' media='all' />
<link rel='stylesheet' id='yith_wccl_frontend-css'  href='https://order.jamscatering.com.ua/wp-content/plugins/yith-woocommerce-product-add-ons/assets/css/yith-wccl.css?ver=1.5.12' type='text/css' media='all' />
<style id='yith_wccl_frontend-inline-css' type='text/css'>
.select_option .yith_wccl_tooltip > span{background: ;color: ;}
            .select_option .yith_wccl_tooltip.bottom span:after{border-bottom-color: ;}
            .select_option .yith_wccl_tooltip.top span:after{border-top-color: ;}
</style>
<link rel='stylesheet' id='bootstrap-css'  href='https://order.jamscatering.com.ua/wp-content/themes/pizzaro/assets/css/bootstrap.min.css?ver=3.3.7' type='text/css' media='all' />
<link rel='stylesheet' id='animate-css'  href='https://order.jamscatering.com.ua/wp-content/themes/pizzaro/assets/css/animate.min.css?ver=3.5.1' type='text/css' media='all' />
<link rel='stylesheet' id='pizzaro-style-css'  href='https://order.jamscatering.com.ua/wp-content/themes/pizzaro/style.css?ver=1.2.11' type='text/css' media='all' />
<link rel='stylesheet' id='pizzaro-fonts-css'  href='https://fonts.googleapis.com/css?family=Open%20Sans:400italic,400,300,600,700,800|Yanone+Kaffeesatz:400,700,300,200&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
<link rel='stylesheet' id='pizzaro-icons-css'  href='https://order.jamscatering.com.ua/wp-content/themes/pizzaro/assets/css/font-pizzaro.css?ver=4.9.9' type='text/css' media='all' />
<link rel='stylesheet' id='custom-scrollbar-css'  href='https://order.jamscatering.com.ua/wp-content/themes/pizzaro/assets/css/jquery.mCustomScrollbar.min.css?ver=3.1.5' type='text/css' media='all' />
<link rel='stylesheet' id='pizzaro-jetpack-style-css'  href='https://order.jamscatering.com.ua/wp-content/themes/pizzaro/assets/css/jetpack/jetpack.css?ver=1.2.11' type='text/css' media='all' />
<link rel='stylesheet' id='woothemes-features-layout-css'  href='https://order.jamscatering.com.ua/wp-content/plugins/features-by-woothemes/assets/css/layout.css?ver=1.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='pizzaro-child-style-css'  href='https://order.jamscatering.com.ua/wp-content/themes/pizzaro-child/style.css?ver=4.9.9' type='text/css' media='all' />
<link rel='stylesheet' id='jquery-ui-css'  href='https://order.jamscatering.com.ua/wp-content/plugins/yith-woocommerce-product-add-ons/assets/css/jquery-ui.min.css?ver=1.11.4' type='text/css' media='all' />
<link rel='stylesheet' id='yith_wapo_frontend-css'  href='https://order.jamscatering.com.ua/wp-content/plugins/yith-woocommerce-product-add-ons/assets/css/yith-wapo.css?ver=1.5.12' type='text/css' media='all' />
<style id='yith_wapo_frontend-inline-css' type='text/css'>

			.wapo_option_tooltip .yith_wccl_tooltip > span {
				background: #222222;
				color: #ffffff;
			}
			.wapo_option_tooltip .yith_wccl_tooltip.bottom span:after {
				border-bottom-color: #222222;
			}
			.wapo_option_tooltip .yith_wccl_tooltip.top span:after {
				border-top-color: #222222;
			}
</style>
<link rel='stylesheet' id='yith_wapo_frontend-colorpicker-css'  href='https://order.jamscatering.com.ua/wp-content/plugins/yith-woocommerce-product-add-ons/assets/css/color-picker.min.css?ver=1.5.12' type='text/css' media='all' />
<link rel='stylesheet' id='kc-general-css'  href='https://order.jamscatering.com.ua/wp-content/plugins/kingcomposer/assets/frontend/css/kingcomposer.min.css?ver=2.7.6' type='text/css' media='all' />
<link rel='stylesheet' id='kc-animate-css'  href='https://order.jamscatering.com.ua/wp-content/plugins/kingcomposer/assets/css/animate.css?ver=2.7.6' type='text/css' media='all' />
<link rel='stylesheet' id='kc-icon-1-css'  href='https://order.jamscatering.com.ua/wp-content/plugins/kingcomposer/assets/css/icons.css?ver=2.7.6' type='text/css' media='all' />
<script type="text/template" id="tmpl-variation-template">
	<div class="woocommerce-variation-description">{{{ data.variation.variation_description }}}</div>
	<div class="woocommerce-variation-price">{{{ data.variation.price_html }}}</div>
	<div class="woocommerce-variation-availability">{{{ data.variation.availability_html }}}</div>
</script>
<script type="text/template" id="tmpl-unavailable-variation-template">
	<p>На жаль, цей товар недоступний. Будь ласка, виберіть інше поєднання.</p>
</script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.4.8'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.8'></script>
<link rel='https://api.w.org/' href='https://order.jamscatering.com.ua/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://order.jamscatering.com.ua/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://order.jamscatering.com.ua/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.9.9" />
<meta name="generator" content="WooCommerce 3.5.1" />
<script type="text/javascript">var kc_script_data={ajax_url:"https://order.jamscatering.com.ua/wp-admin/admin-ajax.php"}</script><meta name="tec-api-version" content="v1"><meta name="tec-api-origin" content="https://order.jamscatering.com.ua"><link rel="https://theeventscalendar.com/" href="https://order.jamscatering.com.ua/wp-json/tribe/events/v1/" />	<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
			<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		<style type="text/css" media="print">#wpadminbar { display:none; }</style>
<style type="text/css" media="screen">
	html { margin-top: 32px !important; }
	* html body { margin-top: 32px !important; }
	@media screen and ( max-width: 782px ) {
		html { margin-top: 46px !important; }
		* html body { margin-top: 46px !important; }
	}
</style>
<meta name="generator" content="Powered by Slider Revolution 5.4.8 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<link rel="icon" href="https://order.jamscatering.com.ua/wp-content/uploads/2018/11/fav.png" sizes="32x32" />
<link rel="icon" href="https://order.jamscatering.com.ua/wp-content/uploads/2018/11/fav.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="https://order.jamscatering.com.ua/wp-content/uploads/2018/11/fav.png" />
<meta name="msapplication-TileImage" content="https://order.jamscatering.com.ua/wp-content/uploads/2018/11/fav.png" />
<script type="text/javascript">function setREVStartSize(e){									
						try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
							if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})					
						}catch(d){console.log("Failure at Presize of Slider:"+d)}						
					};</script>
<style type="text/css">
		button,
		.button,
		#scrollUp,
		.header-v1,
		.header-v2,
		.header-v3,
		.header-v4,
		.header-v5,
		.added_to_cart,
		.header-v1 .stuck,
		.header-v2 .stuck,
		.header-v3 .stuck,
		.header-v4 .stuck,
		.header-v5 .stuck,
		input[type="reset"],
		input[type="submit"],
		input[type="button"],
		.dark .create-your-own a,
		.owl-dots .owl-dot.active,
		.pizzaro-handheld-footer-bar,
		.widget_nav_menu .menu li:hover,
		.related > h2:first-child:after,
		.upsells > h2:first-child:after,
		.widget_nav_menu .menu li::after,
		.section-products .section-title:after,
		.pizzaro-handheld-footer-bar ul li > a,
		.banners .banner .caption .banner-price,
		.section-tabs .nav .nav-item.active a::after,
		.products-with-gallery-tabs.section-tabs .nav,
		.section-recent-post .post-info .btn-more:hover,
		.section-sale-product .price-action .button:hover,
		.list-no-image-view ul.products li.product::before,
		.woocommerce-account .customer-login-form h2::after,
		.section-coupon .caption .coupon-info .button:hover,
		.page-template-template-homepage-v2 .header-v2 .stuck,
		.woocommerce-cart .pizzaro-order-steps ul .cart .step,
		.list-no-image-cat-view ul.products li.product::before,
		.pizzaro-handheld-footer-bar ul li.search .site-search,
		.widget.widget_price_filter .ui-slider .ui-slider-handle,
		.list-no-image-view .products .owl-item>.product::before,
		.list-no-image-view ul.products li.product .button:hover,
		.woocommerce-checkout .pizzaro-order-steps ul .cart .step,
		.woocommerce-cart .cart-collaterals+.cross-sells h2::after,
		.footer-v1.site-footer .site-address .address li+li::before,
		.list-no-image-cat-view ul.products li.product .button:hover,
		.header-v4.lite-bg .primary-navigation .menu > li > a::before,
		.woocommerce-checkout .pizzaro-order-steps ul .checkout .step,
		.kc-section-tab.kc_tabs .kc_tabs_nav>.ui-tabs-active>a::after,
		.list-no-image-view .products .owl-item>.product .button:hover,
		.pizzaro-sidebar-header .footer-social-icons ul li a:hover,
		.list-view.left-sidebar.columns-1 ul.products li.product .button:hover,
		.products-card .media .media-left ul.products li.product .button:hover,
		.products-card .media .media-right ul.products li.product .button:hover,
		.pizzaro-sidebar-header .primary-navigation > ul > li:hover,
		.list-view.right-sidebar.columns-1 ul.products li.product .button:hover,
		.pizzaro-sidebar-header .secondary-navigation .menu li:hover,
		.pizzaro-sidebar-header .secondary-navigation .menu li::after,
		.pizzaro-sidebar-header .main-navigation ul.menu ul li:hover > a,
		.list-view.left-sidebar.columns-1 .products .owl-item>.product .button:hover,
		.list-view.right-sidebar.columns-1 .products .owl-item>.product .button:hover,
		.woocommerce-order-received.woocommerce-checkout .pizzaro-order-steps ul .step,
		.pizzaro-sidebar-header .main-navigation ul.nav-menu ul li:hover > a,
		.page-template-template-homepage-v2 .products-with-gallery-tabs.section-tabs .nav,
		.stretch-full-width .store-locator .store-search-form form .button,
		.banner.social-block .caption .button:hover,
		.wpsl-search #wpsl-search-btn,
		.lite-bg.header-v4 .primary-navigation .menu .current-menu-item>a::before {
			background-color: #861a53;
		}

		.custom .tp-bullet.selected,
		.home-v1-slider .btn-primary,
		.home-v2-slider .btn-primary,
		.home-v3-slider .btn-primary,
		.products-with-gallery-tabs.kc_tabs>.kc_wrapper>.kc_tabs_nav,
		.products-with-gallery-tabs.kc_tabs .kc_tabs_nav li.ui-tabs-active a::after {
			background-color: #861a53 !important;
		}

		.lite-bg.header-v4 #pizzaro-logo,
		.lite-bg.header-v3 #pizzaro-logo,
		.site-footer.footer-v5 #pizzaro-logo,
		.site-footer.footer-v4 .footer-logo #pizzaro-logo,
		.page-template-template-homepage-v4 .header-v3 #pizzaro-logo,
		.pizzaro-sidebar-header .header-v1 .site-branding #pizzaro-logo,
		.pizzaro-sidebar-header .header-v2 .site-branding #pizzaro-logo,
		.pizzaro-sidebar-header .header-v3 .site-branding #pizzaro-logo,
		.pizzaro-sidebar-header .header-v4 .site-branding #pizzaro-logo,
		.pizzaro-sidebar-header .header-v5 .site-branding #pizzaro-logo,
		.pizzaro-sidebar-header .header-v6 .site-branding #pizzaro-logo {
			fill: #861a53;
		}

		.section-events .section-title,
		.section-product-categories .section-title,
		.section-products-carousel-with-image .section-title,
		.section-product .product-wrapper .product-inner header .sub-title,
		.section-recent-post .post-info .btn-more,
		.section-coupon .caption .coupon-code,
		.widget_layered_nav li:before,
		.product_list_widget .product-title,
		.product_list_widget li>a,
		#payment .payment_methods li label a:hover,
		article.post.format-link .entry-content p a,
		.page-template-template-contactpage .store-info a {
			color: #861a53;
		}

		.section-recent-posts .section-title,
		.terms-conditions .entry-content .section.contact-us p a {
			color: #60133c;
		}

		.secondary-navigation ul.menu>li>a, .secondary-navigation ul.nav-menu>li>a {
			color: #10733a3;
		}

		button,
		input[type="button"],
		input[type="reset"],
		input[type="submit"],
		.button,
		.added_to_cart,
		.section-sale-product .price-action .button:hover,
		.section-recent-post .post-info .btn-more,
		.section-recent-post .post-info .btn-more:hover,
		.section-coupon .caption .coupon-info .button:hover,
		.widget.widget_price_filter .ui-slider .ui-slider-handle:last-child,
		#order_review_heading::after,
		#customer_details .woocommerce-billing-fields h3::after,
		#customer_details .woocommerce-shipping-fields h3::after,
		.woocommerce-cart .pizzaro-order-steps ul .cart .step,
		.woocommerce-checkout .pizzaro-order-steps ul .checkout .step,
		.pizzaro-sidebar-header .footer-social-icons ul li a:hover,
		.woocommerce-order-received.woocommerce-checkout .pizzaro-order-steps ul .complete .step,
		.tc-extra-product-options .cpf-type-range ul li.tmcp-field-wrap .tm-range-picker .noUi-origin .noUi-handle,
		.cart-collaterals h2::after,
		.widget_nav_menu .menu li:hover a,
		.page-template-template-contactpage .contact-form h2:after,
		.page-template-template-contactpage .store-info h2:after,
		.banner.social-block .caption .button:hover,
		#byconsolewooodt_checkout_field h2::after {
			border-color: #861a53;
		}

		.pizzaro-order-steps ul .step {
			border-color: #ac216a;
		}

		button,
		.button:hover,
		.added_to_cart:hover,
		#respond input[type=submit],
		input[type="button"]:hover,
		input[type="reset"]:hover,
		input[type="submit"]:hover,
		.dark .create-your-own a:hover,
		.wc-proceed-to-checkout .button,
		.main-navigation ul.menu ul a:hover,
		.main-navigation ul.menu ul li:hover>a,
		.main-navigation ul.nav-menu ul a:hover,
		.main-navigation ul.nav-menu ul li:hover>a,
		.main-navigation div.menu ul.nav-menu ul a:hover,
		.main-navigation div.menu ul.nav-menu ul li:hover>a,
		.stretch-full-width .store-locator .store-search-form form .button:hover {
		    background-color: #711646;
		}

		#respond input[type=submit]:hover {
		    background-color: #66143f;
		}

		.footer-v1.site-footer .footer-action-btn {
		    background-color: #a62067;
		}
		
		.single-product div.product .woocommerce-product-gallery .flex-control-thumbs li img.flex-active,
		.single-product.style-2 div.product .summary .pizzaro-wc-product-gallery .pizzaro-wc-product-gallery__wrapper .pizzaro-wc-product-gallery__image.flex-active-slide {
		    border-bottom-color: #861a53;
		}

		@media (max-width: 1025px) {
			.page-template-template-homepage-v2 .header-v2 {
				background-color: #861a53;
			}
		}</style>		<style type="text/css" id="wp-custom-css">
			.widget .widget-title{
	text-align:center;
	font-weight: 700;
	padding-bottom:10px;
}
.products .owl-item > .product, ul.products li.product:hover h2.woocommerce-loop-product__title{
	height:auto;
}
header#masthead{
	z-index:9999999999;
}
/* .hover-area{
	display:none!important;
} */
li.payment_method_cod, li.payment_method_liqpay {
	display:none;
}
ul.wc_payment_methods {
	padding:0!important;
}
.shop-archive-header{
	background:#861a53!important;
}
.footer-action-btn{
	display:none!important;
}
.footer-v1.site-footer .site-info{
	margin-bottom:0!important;
}
li.outofstock .product-image-wrapper img{
	 -webkit-filter: grayscale(100%);
  -moz-filter: grayscale(100%);
  -ms-filter: grayscale(100%);
  -o-filter: grayscale(100%);
  filter: grayscale(100%);
  filter: gray; /* IE 6-9 */
}
.modal{
	z-index:99999999999;
}
#change-order-date{
	margin:0 auto 20px!important;
}
.modal-content .md-form {
	display:flex;
}
.modal-content .date-picker{
	margin-left:2%;
	margin-top: 4px;
}
.modal-content #submit-order-date{
		margin-left:2%;
		margin-right:2%;
	    margin-bottom: 10px;
    padding: 8px 44px;
	font-size:14px;
}
.modal-content .modal-header{
	border:none;
}
@media(max-width:1200px){
	ul.products li.product .product-outer{
		height:auto!important;
	}
	.product-skl{
		text-align:center;
	}
	h2.woocommerce-loop-product__title{
	height:auto!important;
}
}
@media(max-width: 1024px){
	.header-v2 .site-branding a img{
		width:55px;
	}
	.site-content{
	padding-top:50px;
	}
	.content-area #change-order-date{
		padding:15px 35px;
	}
	.header-v2 .site-branding a img{
		margin:0;
	}
}
.left-sidebar.columns-3 ul.products li.product .product-content-wrapper, .left-sidebar.columns-3 ul.products li.product .product-image-wrapper, .right-sidebar.columns-3 ul.products li.product .product-content-wrapper, .right-sidebar.columns-3 ul.products li.product .product-image-wrapper{
	border:none;
}

.products .owl-item>.product.hover .product-inner, .products .owl-item>.product:hover .product-inner, ul.products li.product.hover .product-inner, ul.products li.product:hover .product-inner{
	box-shadow: 0px 0px 8px 4px #ececec;
}
.woocommerce-mini-cart__empty-message{
	text-align:center;
}
.summary  .woocommerce-Price-amount{
	display:none;
}
.single-product div.product .summary .product_title, .yith_wapo_groups_container{
	margin-bottom:0;
}
.single-product div.product form.cart{
	padding-top:0;
}
.woocommerce-Price-amount{
	    font-size: 22px;
}
ul.products li.product .woocommerce-LoopProduct-link>h3{
	height: auto;
    line-height: 20px;
    font-size: 16px;
    padding: 0;
    margin: 0;
    border: none;
}
.left-sidebar.columns-3 ul.products li.product .product-content-wrapper{
	padding-left:0;
padding-right:0;
}
h2.woocommerce-loop-product__title{

	    display: block!important;
    text-align: center;
    margin: 0 auto 10px auto;
}
@media (max-width:380px){
	.modal-content .md-form{
		flex-wrap:wrap;
	}
	.modal-content .date-picker{
		margin-right:2%;

	}
	.modal-content #submit-order-date{
		margin:10px auto;
	}
	.form-control{
		width:100%!important;
	}
}		</style>
	</head>

<body class="archive post-type-archive post-type-archive-product logged-in admin-bar no-customize-support wp-custom-logo kc-css-system woocommerce woocommerce-page woocommerce-no-js tribe-no-js yith-wapo-frontend left-sidebar woocommerce-active lite grid-view columns-3">
<div id="page" class="hfeed site">
	
	
	<header id="masthead" class="site-header header-v2 " role="banner" style="background-image: none; ">
		<div class="site-header-wrap">
		<div class="col-full">

					<a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a>
		<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
					<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="Primary Navigation">
				<button class="menu-toggle" aria-controls="site-navigation" aria-expanded="false"><span class="close-icon"><i class="po po-close-delete"></i></span><span class="menu-icon"><i class="po po-menu-icon"></i></span><span class=" screen-reader-text">Menu</span></button>

				<div class="primary-navigation">
					<ul id="menu-main-menu" class="menu"><li id="menu-item-1100" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item current_page_item menu-item-1100"><a href="https://order.jamscatering.com.ua/shop/">Всі страви</a></li>
</ul>				</div>

				<div class="handheld-navigation">
					<span class="phm-close">Закрыть</span><ul id="menu-food-menu" class="menu"><li id="menu-item-1123" class="soup_class menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1123"><a href="https://order.jamscatering.com.ua/product-category/pershi-stravy/">Перші Страви</a></li>
<li id="menu-item-1124" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1124"><a href="https://order.jamscatering.com.ua/product-category/salaty/"><i class="po po-salads"></i>Салати</a></li>
<li id="menu-item-1122" class="meat_class menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1122"><a href="https://order.jamscatering.com.ua/product-category/osnovni-stravy/">Основні Страви</a></li>
<li id="menu-item-1001" class="garnir_class menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1001"><a href="https://order.jamscatering.com.ua/product-category/garniry/">Гарніри</a></li>
<li id="menu-item-1130" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1130"><a href="https://order.jamscatering.com.ua/product-category/napo%d1%97/"><i class="po po-drinks"></i>Напої</a></li>
<li id="menu-item-1249" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1249"><a href="https://order.jamscatering.com.ua/product-category/fast-food/"><i class="po po-burger"></i>Fast Food</a></li>
<li id="menu-item-1397" class="sous_class menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1397"><a href="https://order.jamscatering.com.ua/product-category/dodatki/">Додатки</a></li>
<li id="menu-item-1422" class="desert_class menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1422"><a href="https://order.jamscatering.com.ua/product-category/deserti/">Десерти</a></li>
</ul>				</div>

			</nav><!-- #site-navigation -->
					<div class="site-branding">
			<a href="https://order.jamscatering.com.ua/" class="custom-logo-link" rel="home" itemprop="url"><img width="109" height="109" src="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/jam-catering-logo3.png" class="custom-logo" alt="Jam&#039;s catering" itemprop="logo" srcset="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/jam-catering-logo3.png 109w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/jam-catering-logo3-100x100.png 100w" sizes="(max-width: 109px) 100vw, 109px" /></a>		</div>
		<div class="header-info-wrapper">		<div class="header-nav-links">
							<ul>
											<li>
														<a class="my-account" href="https://order.jamscatering.com.ua/my-account/">
								<i class=""></i>
								Мій кабінет							</a>
						</li>
									</ul>
					</div>
					<ul class="site-header-cart menu">
				<li class="mini-cart">
					<span class="mini-cart-toggle">
									<a class="cart-contents" href="https://order.jamscatering.com.ua/cart/" title="Перегляд кошика">
				<span class="amount"><span class="price-label">Ваш кошик:</span>&#8372;38.00</span> <span class="count">2</span>
			</a>
								<div class="widget woocommerce widget_shopping_cart"><div class="widget_shopping_cart_content"></div></div>					</span>
				</li>
			</ul>
			</div>
		</div>
		</div>
	</header><!-- #masthead -->

	
	<div id="content" class="site-content" tabindex="-1" >
		<div class="col-full">

		<div class="pizzaro-sorting"><form class="woocommerce-ordering" method="get">
	<select name="orderby" class="orderby">
					<option value="popularity"  selected='selected'>Сортувати за популярністю</option>
					<option value="rating" >Сортувати за оцінкою</option>
					<option value="date" >Сортувати за останніми</option>
					<option value="price" >Сортувати за ціною: від нижчої до вищої</option>
					<option value="price-desc" >Сортувати за ціною: від вищої до нижчої</option>
			</select>
	<input type="hidden" name="paged" value="1" />
	</form>
<form method="POST" action="" class="form-pizzaro-wc-ppp"><select name="ppp" onchange="this.form.submit()" class="pizzaro-wc-wppp-select c-select"><option value="12"  selected='selected'>Показати 12</option><option value="24" >Показати 24</option><option value="48" >Показати 48</option><option value="96" >Показати 96</option><option value="-1" >Показати Все</option></select></form><p class="woocommerce-result-count">
	Відображається 1-12 з 98 результатів</p>
</div>
		<div id="primary" class="content-area">
        <button id="change-order-date" style="display: none; margin-left: 25%">Обрати іншу дату замовлення</button>
			<main id="main" class="site-main" role="main">
		<div class="woocommerce"></div><header class="woocommerce-products-header">
	
	</header>
<div class="woocommerce-notices-wrapper"></div><ul class="products columns-3">
<li class="post-290 product type-product status-publish has-post-thumbnail product_cat-pershi-stravy first instock taxable shipping-taxable purchasable product-type-simple">
	<div class="product-outer"><div class="product-inner">		<div class="product-image-wrapper">
		<a href="https://order.jamscatering.com.ua/product/borsch-chervonii/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="300" src="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Борщ-червоний-1-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Борщ-червоний-1-300x300.jpg 300w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Борщ-червоний-1-150x150.jpg 150w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Борщ-червоний-1-100x100.jpg 100w" sizes="(max-width: 300px) 100vw, 300px" /></a>		</div>
				<div class="product-content-wrapper">
		<a href="https://order.jamscatering.com.ua/product/borsch-chervonii/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><h2 class="woocommerce-loop-product__title">Борщ червоний</h2><h3 itemprop="name" class="product_title entry-title">Вага: 250 гр</h3><div class="star-rating"><span style="width:100%">Оцінено в <strong class="rating">5.00</strong> з 5</span></div>
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#8372;</span>29.00</span></span>
</a><div class="hover-area"><a href="/shop/?add-to-cart=290" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="290" data-product_sku="" aria-label="Додайте &ldquo;Борщ червоний&rdquo; до кошика" rel="nofollow">Додати у кошик</a></div>		</div>
		</div><!-- /.product-inner --></div><!-- /.product-outer --></li>
<li class="post-1407 product type-product status-publish product_cat-dodatki instock taxable shipping-taxable purchasable product-type-simple">
	<div class="product-outer"><div class="product-inner">		<div class="product-image-wrapper">
		<a href="https://order.jamscatering.com.ua/product/bulochka/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img src="https://order.jamscatering.com.ua/wp-content/plugins/woocommerce/assets/images/placeholder.png" alt="Заповнювач" width="300" class="woocommerce-placeholder wp-post-image" height="300" /></a>		</div>
				<div class="product-content-wrapper">
		<a href="https://order.jamscatering.com.ua/product/bulochka/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><h2 class="woocommerce-loop-product__title">Булочка</h2><h3 itemprop="name" class="product_title entry-title">Вага: </h3>
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#8372;</span>4.00</span></span>
</a><div class="hover-area"><a href="/shop/?add-to-cart=1407" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="1407" data-product_sku="" aria-label="Додайте &ldquo;Булочка&rdquo; до кошика" rel="nofollow">Додати у кошик</a></div>		</div>
		</div><!-- /.product-inner --></div><!-- /.product-outer --></li>
<li class="post-1285 product type-product status-publish has-post-thumbnail product_cat-pershi-stravy last instock taxable shipping-taxable purchasable product-type-simple">
	<div class="product-outer"><div class="product-inner">		<div class="product-image-wrapper">
		<a href="https://order.jamscatering.com.ua/product/bulion-kuryachii/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="300" src="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бульйон-курячий-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бульйон-курячий-300x300.jpg 300w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бульйон-курячий-150x150.jpg 150w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бульйон-курячий-100x100.jpg 100w" sizes="(max-width: 300px) 100vw, 300px" /></a>		</div>
				<div class="product-content-wrapper">
		<a href="https://order.jamscatering.com.ua/product/bulion-kuryachii/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><h2 class="woocommerce-loop-product__title">Бульйон курячий</h2><h3 itemprop="name" class="product_title entry-title">Вага: 250 гр</h3>
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#8372;</span>26.00</span></span>
</a><div class="hover-area"><a href="/shop/?add-to-cart=1285" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="1285" data-product_sku="" aria-label="Додайте &ldquo;Бульйон курячий&rdquo; до кошика" rel="nofollow">Додати у кошик</a></div>		</div>
		</div><!-- /.product-inner --></div><!-- /.product-outer --></li>
<li class="post-288 product type-product status-publish has-post-thumbnail product_cat-fast-food first instock taxable shipping-taxable purchasable product-type-simple">
	<div class="product-outer"><div class="product-inner">		<div class="product-image-wrapper">
		<a href="https://order.jamscatering.com.ua/product/burger-z-kuryatinoyu/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="300" src="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-курятиною-130..29-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-курятиною-130..29-300x300.jpg 300w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-курятиною-130..29-150x150.jpg 150w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-курятиною-130..29-768x768.jpg 768w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-курятиною-130..29-1024x1024.jpg 1024w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-курятиною-130..29-600x600.jpg 600w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-курятиною-130..29-100x100.jpg 100w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-курятиною-130..29.jpg 1280w" sizes="(max-width: 300px) 100vw, 300px" /></a>		</div>
				<div class="product-content-wrapper">
		<a href="https://order.jamscatering.com.ua/product/burger-z-kuryatinoyu/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><h2 class="woocommerce-loop-product__title">Бургер з курятиною</h2><h3 itemprop="name" class="product_title entry-title">Вага: 170 гр</h3>
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#8372;</span>56.00</span></span>
</a><div class="hover-area"><a href="/shop/?add-to-cart=288" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="288" data-product_sku="" aria-label="Додайте &ldquo;Бургер з курятиною&rdquo; до кошика" rel="nofollow">Додати у кошик</a></div>		</div>
		</div><!-- /.product-inner --></div><!-- /.product-outer --></li>
<li class="post-286 product type-product status-publish has-post-thumbnail product_cat-fast-food instock taxable shipping-taxable purchasable product-type-simple">
	<div class="product-outer"><div class="product-inner">		<div class="product-image-wrapper">
		<a href="https://order.jamscatering.com.ua/product/burger-z-lososem/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="300" src="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-лососем-100..47-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-лососем-100..47-300x300.jpg 300w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-лососем-100..47-150x150.jpg 150w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-лососем-100..47-768x768.jpg 768w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-лососем-100..47-1024x1024.jpg 1024w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-лососем-100..47-600x600.jpg 600w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-лососем-100..47-100x100.jpg 100w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-лососем-100..47.jpg 1280w" sizes="(max-width: 300px) 100vw, 300px" /></a>		</div>
				<div class="product-content-wrapper">
		<a href="https://order.jamscatering.com.ua/product/burger-z-lososem/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><h2 class="woocommerce-loop-product__title">Бургер з лососем</h2><h3 itemprop="name" class="product_title entry-title">Вага: 170 гр</h3>
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#8372;</span>73.00</span></span>
</a><div class="hover-area"><a href="/shop/?add-to-cart=286" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="286" data-product_sku="" aria-label="Додайте &ldquo;Бургер з лососем&rdquo; до кошика" rel="nofollow">Додати у кошик</a></div>		</div>
		</div><!-- /.product-inner --></div><!-- /.product-outer --></li>
<li class="post-191 product type-product status-publish has-post-thumbnail product_cat-fast-food last instock taxable shipping-taxable purchasable product-type-simple">
	<div class="product-outer"><div class="product-inner">		<div class="product-image-wrapper">
		<a href="https://order.jamscatering.com.ua/product/burger-z-telyatinoyu/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="300" src="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-телятиною-130..39-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-телятиною-130..39-300x300.jpg 300w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-телятиною-130..39-150x150.jpg 150w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-телятиною-130..39-768x768.jpg 768w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-телятиною-130..39-1024x1024.jpg 1024w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-телятиною-130..39-600x600.jpg 600w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-телятиною-130..39-100x100.jpg 100w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-телятиною-130..39.jpg 1280w" sizes="(max-width: 300px) 100vw, 300px" /></a>		</div>
				<div class="product-content-wrapper">
		<a href="https://order.jamscatering.com.ua/product/burger-z-telyatinoyu/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><h2 class="woocommerce-loop-product__title">Бургер з телятиною</h2><h3 itemprop="name" class="product_title entry-title">Вага: 130 гр</h3><div class="woocommerce-product-details__short-description">
	<p>A mighty meaty double helping of all the reasons you love our burger.</p>
</div>

	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#8372;</span>67.00</span></span>
</a><div class="hover-area"><a href="/shop/?add-to-cart=191" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="191" data-product_sku="" aria-label="Додайте &ldquo;Бургер з телятиною&rdquo; до кошика" rel="nofollow">Додати у кошик</a></div>		</div>
		</div><!-- /.product-inner --></div><!-- /.product-outer --></li>
<li class="post-189 product type-product status-publish has-post-thumbnail product_cat-fast-food first instock taxable shipping-taxable purchasable product-type-simple">
	<div class="product-outer"><div class="product-inner">		<div class="product-image-wrapper">
		<a href="https://order.jamscatering.com.ua/product/burger-z-shinkoyu/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="300" src="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-шинкою-130..32-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-шинкою-130..32-300x300.jpg 300w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-шинкою-130..32-150x150.jpg 150w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-шинкою-130..32-768x768.jpg 768w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-шинкою-130..32-1024x1024.jpg 1024w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-шинкою-130..32-600x600.jpg 600w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-шинкою-130..32-100x100.jpg 100w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-шинкою-130..32.jpg 1280w" sizes="(max-width: 300px) 100vw, 300px" /></a>		</div>
				<div class="product-content-wrapper">
		<a href="https://order.jamscatering.com.ua/product/burger-z-shinkoyu/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><h2 class="woocommerce-loop-product__title">Бургер з шинкою</h2><h3 itemprop="name" class="product_title entry-title">Вага: 130 гр</h3><div class="woocommerce-product-details__short-description">
	<p>A mighty meaty double helping of all the reasons you love our burger.</p>
</div>

	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#8372;</span>35.00</span></span>
</a><div class="hover-area"><a href="/shop/?add-to-cart=189" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="189" data-product_sku="" aria-label="Додайте &ldquo;Бургер з шинкою&rdquo; до кошика" rel="nofollow">Додати у кошик</a></div>		</div>
		</div><!-- /.product-inner --></div><!-- /.product-outer --></li>
<li class="post-262 product type-product status-publish has-post-thumbnail product_cat-fast-food instock taxable shipping-taxable purchasable product-type-simple">
	<div class="product-outer"><div class="product-inner">		<div class="product-image-wrapper">
		<a href="https://order.jamscatering.com.ua/product/burger-z-svininoyu/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="300" src="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-свининою-130..34-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-свининою-130..34-300x300.jpg 300w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-свининою-130..34-150x150.jpg 150w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-свининою-130..34-768x768.jpg 768w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-свининою-130..34-1024x1024.jpg 1024w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-свининою-130..34-600x600.jpg 600w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-свининою-130..34-100x100.jpg 100w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бургер-з-свининою-130..34.jpg 1280w" sizes="(max-width: 300px) 100vw, 300px" /></a>		</div>
				<div class="product-content-wrapper">
		<a href="https://order.jamscatering.com.ua/product/burger-z-svininoyu/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><h2 class="woocommerce-loop-product__title">Бургер зі свининою</h2><h3 itemprop="name" class="product_title entry-title">Вага: 170 гр</h3><div class="woocommerce-product-details__short-description">
	<p>A mighty meaty double helping of all the reasons you love our burger.</p>
</div>

	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#8372;</span>59.00</span></span>
</a><div class="hover-area"><a href="/shop/?add-to-cart=262" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="262" data-product_sku="" aria-label="Додайте &ldquo;Бургер зі свининою&rdquo; до кошика" rel="nofollow">Додати у кошик</a></div>		</div>
		</div><!-- /.product-inner --></div><!-- /.product-outer --></li>
<li class="post-1340 product type-product status-publish has-post-thumbnail product_cat-salaty last instock taxable shipping-taxable purchasable product-type-simple">
	<div class="product-outer"><div class="product-inner">		<div class="product-image-wrapper">
		<a href="https://order.jamscatering.com.ua/product/buryachkovii-z-kvashenim-og%d1%96rkom/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="300" src="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бурячковий-з-квашеним-огірком-150...17-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бурячковий-з-квашеним-огірком-150...17-300x300.jpg 300w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бурячковий-з-квашеним-огірком-150...17-150x150.jpg 150w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бурячковий-з-квашеним-огірком-150...17-100x100.jpg 100w" sizes="(max-width: 300px) 100vw, 300px" /></a>		</div>
				<div class="product-content-wrapper">
		<a href="https://order.jamscatering.com.ua/product/buryachkovii-z-kvashenim-og%d1%96rkom/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><h2 class="woocommerce-loop-product__title">Бурячковий з квашеним огірком</h2><h3 itemprop="name" class="product_title entry-title">Вага: 130 гр</h3>
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#8372;</span>34.00</span></span>
</a><div class="hover-area"><a href="/shop/?add-to-cart=1340" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="1340" data-product_sku="" aria-label="Додайте &ldquo;Бурячковий з квашеним огірком&rdquo; до кошика" rel="nofollow">Додати у кошик</a></div>		</div>
		</div><!-- /.product-inner --></div><!-- /.product-outer --></li>
<li class="post-1015 product type-product status-publish has-post-thumbnail product_cat-salaty first instock taxable shipping-taxable purchasable product-type-simple">
	<div class="product-outer"><div class="product-inner">		<div class="product-image-wrapper">
		<a href="https://order.jamscatering.com.ua/product/buryachkovii-z-chornoslivom/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="300" src="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бурячковий-з-чорносливом-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бурячковий-з-чорносливом-300x300.jpg 300w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бурячковий-з-чорносливом-150x150.jpg 150w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Бурячковий-з-чорносливом-100x100.jpg 100w" sizes="(max-width: 300px) 100vw, 300px" /></a>		</div>
				<div class="product-content-wrapper">
		<a href="https://order.jamscatering.com.ua/product/buryachkovii-z-chornoslivom/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><h2 class="woocommerce-loop-product__title">Бурячковий з чорносливом</h2><h3 itemprop="name" class="product_title entry-title">Вага: 150 гр</h3>
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#8372;</span>15.00</span></span>
</a><div class="hover-area"><a href="/shop/?add-to-cart=1015" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="1015" data-product_sku="" aria-label="Додайте &ldquo;Бурячковий з чорносливом&rdquo; до кошика" rel="nofollow">Додати у кошик</a></div>		</div>
		</div><!-- /.product-inner --></div><!-- /.product-outer --></li>
<li class="post-1146 product type-product status-publish has-post-thumbnail product_cat-osnovni-stravy instock taxable shipping-taxable purchasable product-type-simple">
	<div class="product-outer"><div class="product-inner">		<div class="product-image-wrapper">
		<a href="https://order.jamscatering.com.ua/product/v%d1%96dbivna-kuryacha/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="300" src="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Відбивна-куряча-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Відбивна-куряча-300x300.jpg 300w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Відбивна-куряча-150x150.jpg 150w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Відбивна-куряча-100x100.jpg 100w" sizes="(max-width: 300px) 100vw, 300px" /></a>		</div>
				<div class="product-content-wrapper">
		<a href="https://order.jamscatering.com.ua/product/v%d1%96dbivna-kuryacha/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><h2 class="woocommerce-loop-product__title">Відбивна-куряча</h2><h3 itemprop="name" class="product_title entry-title">Вага: 120 гр</h3>
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#8372;</span>29.00</span></span>
</a><div class="hover-area"><a href="/shop/?add-to-cart=1146" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="1146" data-product_sku="" aria-label="Додайте &ldquo;Відбивна-куряча&rdquo; до кошика" rel="nofollow">Додати у кошик</a></div>		</div>
		</div><!-- /.product-inner --></div><!-- /.product-outer --></li>
<li class="post-1016 product type-product status-publish has-post-thumbnail product_cat-osnovni-stravy last instock taxable shipping-taxable purchasable product-type-simple">
	<div class="product-outer"><div class="product-inner">		<div class="product-image-wrapper">
		<a href="https://order.jamscatering.com.ua/product/v%d1%96dbivna-svinna/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="300" src="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Відбивна-свинна-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" srcset="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Відбивна-свинна-300x300.jpg 300w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Відбивна-свинна-150x150.jpg 150w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/Відбивна-свинна-100x100.jpg 100w" sizes="(max-width: 300px) 100vw, 300px" /></a>		</div>
				<div class="product-content-wrapper">
		<a href="https://order.jamscatering.com.ua/product/v%d1%96dbivna-svinna/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><h2 class="woocommerce-loop-product__title">Відбивна-свинна</h2><h3 itemprop="name" class="product_title entry-title">Вага: 130 гр</h3>
	<span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#8372;</span>41.00</span></span>
</a><div class="hover-area"><a href="/shop/?add-to-cart=1016" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="1016" data-product_sku="" aria-label="Додайте &ldquo;Відбивна-свинна&rdquo; до кошика" rel="nofollow">Додати у кошик</a></div>		</div>
		</div><!-- /.product-inner --></div><!-- /.product-outer --></li>
</ul>
<nav class="woocommerce-pagination">
	<ul class='page-numbers'>
	<li><span aria-current='page' class='page-numbers current'>1</span></li>
	<li><a class='page-numbers' href='https://order.jamscatering.com.ua/shop/page/2/'>2</a></li>
	<li><a class='page-numbers' href='https://order.jamscatering.com.ua/shop/page/3/'>3</a></li>
	<li><a class='page-numbers' href='https://order.jamscatering.com.ua/shop/page/4/'>4</a></li>
	<li><span class="page-numbers dots">&hellip;</span></li>
	<li><a class='page-numbers' href='https://order.jamscatering.com.ua/shop/page/7/'>7</a></li>
	<li><a class='page-numbers' href='https://order.jamscatering.com.ua/shop/page/8/'>8</a></li>
	<li><a class='page-numbers' href='https://order.jamscatering.com.ua/shop/page/9/'>9</a></li>
	<li><a class="next page-numbers" href="https://order.jamscatering.com.ua/shop/page/2/"> &nbsp;&nbsp;&nbsp;&rarr;</a></li>
</ul>
</nav>
			</main><!-- #main -->
		</div><!-- #primary -->

		
<div id="secondary" class="widget-area" role="complementary">
<div id="nav_menu-1" class="widget widget_nav_menu"><span class="gamma widget-title">Комплексні обіди</span><div class="menu-food-menu-container"><ul id="menu-food-menu-1" class="menu"><li class="soup_class menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1123"><a href="https://order.jamscatering.com.ua/product-category/pershi-stravy/">Перші Страви</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1124"><a href="https://order.jamscatering.com.ua/product-category/salaty/"><i class="po po-salads"></i>Салати</a></li>
<li class="meat_class menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1122"><a href="https://order.jamscatering.com.ua/product-category/osnovni-stravy/">Основні Страви</a></li>
<li class="garnir_class menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1001"><a href="https://order.jamscatering.com.ua/product-category/garniry/">Гарніри</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1130"><a href="https://order.jamscatering.com.ua/product-category/napo%d1%97/"><i class="po po-drinks"></i>Напої</a></li>
<li class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1249"><a href="https://order.jamscatering.com.ua/product-category/fast-food/"><i class="po po-burger"></i>Fast Food</a></li>
<li class="sous_class menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1397"><a href="https://order.jamscatering.com.ua/product-category/dodatki/">Додатки</a></li>
<li class="desert_class menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1422"><a href="https://order.jamscatering.com.ua/product-category/deserti/">Десерти</a></li>
</ul></div></div><div id="woocommerce_product_search-1" class="widget woocommerce widget_product_search"><form role="search" method="get" class="woocommerce-product-search" action="https://order.jamscatering.com.ua/">
	<label class="screen-reader-text" for="woocommerce-product-search-field-0">Шукати:</label>
	<input type="search" id="woocommerce-product-search-field-0" class="search-field" placeholder="Пошук товарів&hellip;" value="" name="s" />
	<button type="submit" value="Пошук">Пошук</button>
	<input type="hidden" name="post_type" value="product" />
</form>
</div><div id="woocommerce_price_filter-1" class="widget woocommerce widget_price_filter"><span class="gamma widget-title">Фільтр за ціною</span><form method="get" action="https://order.jamscatering.com.ua/shop/">
			<div class="price_slider_wrapper">
				<div class="price_slider" style="display:none;"></div>
				<div class="price_slider_amount">
					<input type="text" id="min_price" name="min_price" value="3" data-min="3" placeholder="Мінімальна ціна" />
					<input type="text" id="max_price" name="max_price" value="132" data-max="132" placeholder="Найбільша ціна" />
					<button type="submit" class="button">Фільтр</button>
					<div class="price_label" style="display:none;">
						Ціна: <span class="from"></span> &mdash; <span class="to"></span>
					</div>
					
					<div class="clear"></div>
				</div>
			</div>
		</form></div><div id="woocommerce_products-2" class="widget woocommerce widget_products"><span class="gamma widget-title">Товари</span><ul class="product_list_widget"><li>
	
	<a href="https://order.jamscatering.com.ua/product/r%d1%96zotto/">
		<img width="300" height="300" src="https://order.jamscatering.com.ua/wp-content/uploads/2019/01/44-1-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" />		<span class="product-title">Різотто</span>
	</a>

				
	<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#8372;</span>21.00</span>
	</li><li>
	
	<a href="https://order.jamscatering.com.ua/product/krem-sup-z-lososem/">
		<img width="300" height="300" src="https://order.jamscatering.com.ua/wp-content/uploads/2019/01/49896920_765388917179859_6042402090729340928_n-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" />		<span class="product-title">Крем-суп з лососем</span>
	</a>

				
	<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#8372;</span>132.00</span>
	</li><li>
	
	<a href="https://order.jamscatering.com.ua/product/%d0%bf%d0%b0%d1%81%d1%82%d0%b0-%d0%b1%d0%be%d0%bb%d0%be%d0%bd%d1%8c%d1%94%d0%b7%d0%b5/">
		<img width="300" height="300" src="https://order.jamscatering.com.ua/wp-content/uploads/2019/01/15-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" />		<span class="product-title">Паста Болоньєзе</span>
	</a>

				
	<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#8372;</span>72.00</span>
	</li><li>
	
	<a href="https://order.jamscatering.com.ua/product/%d0%bf%d0%b0%d1%81%d1%82%d0%b0-%d0%ba%d0%b0%d1%80%d0%b1%d0%be%d0%bd%d0%b0%d1%80%d0%b0/">
		<img width="300" height="300" src="https://order.jamscatering.com.ua/wp-content/uploads/2019/01/12-300x300.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail" alt="" />		<span class="product-title">Паста Карбонара</span>
	</a>

				
	<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#8372;</span>69.00</span>
	</li></ul></div></div><!-- /.sidebar-shop -->
		</div><!-- .col-full -->
	</div><!-- #content -->

	

<script src="https://order.jamscatering.com.ua/wp-content/plugins/product-filter/includes/scripts/product_filter.js"></script>
	<footer id="colophon" class="site-footer footer-v1" role="contentinfo">
		<div class="col-full">

						<div class="footer-social-icons">
				<span class="social-icon-text">Follow us</span>
				<ul class="social-icons list-unstyled">
					<li><a class="fa fa-facebook" href="#"></a></li><li><a class="fa fa-twitter" href="#"></a></li>				</ul>
			</div>
			<div class="footer-logo"><a href="https://order.jamscatering.com.ua/" class="custom-logo-link" rel="home" itemprop="url"><img width="109" height="109" src="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/jam-catering-logo3.png" class="custom-logo" alt="Jam&#039;s catering" itemprop="logo" srcset="https://order.jamscatering.com.ua/wp-content/uploads/2018/12/jam-catering-logo3.png 109w, https://order.jamscatering.com.ua/wp-content/uploads/2018/12/jam-catering-logo3-100x100.png 100w" sizes="(max-width: 109px) 100vw, 109px" /></a></div>		<div class="site-address">
			<ul class="address">
								<li></li>
							</ul>
		</div>
				<div class="site-info">
			<p class="copyright">© Jam’s Catering 2018. 
Всі права захищені</p>
		</div><!-- .site-info -->
				<a role="button" class="footer-action-btn" data-toggle="collapse" href="#footer-map-collapse">
			<i class=""></i>
					</a>
				<div class="pizzaro-handheld-footer-bar">
			<ul class="columns-3">
									<li class="my-account">
						<a href="https://order.jamscatering.com.ua/my-account/">Мій кабінет</a>					</li>
									<li class="search">
						<a href="">Search</a>			<div class="site-search">
				<div class="widget woocommerce widget_product_search"><form role="search" method="get" class="woocommerce-product-search" action="https://order.jamscatering.com.ua/">
	<label class="screen-reader-text" for="woocommerce-product-search-field-1">Шукати:</label>
	<input type="search" id="woocommerce-product-search-field-1" class="search-field" placeholder="Пошук товарів&hellip;" value="" name="s" />
	<button type="submit" value="Пошук">Пошук</button>
	<input type="hidden" name="post_type" value="product" />
</form>
</div>			</div>
							</li>
									<li class="cart">
									<a class="footer-cart-contents" href="https://order.jamscatering.com.ua/cart/" title="Перглянути кошик">
				<span class="count">2</span>
			</a>
							</li>
							</ul>
		</div>
		
		</div><!-- .col-full -->
	</footer><!-- #colophon -->

			<div id="footer-map-collapse" class="footer-map collapse"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d317718.69319292053!2d-0.3817765050863085!3d51.528307984912544!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d8a00baf21de75%3A0x52963a5addd52a99!2sLondon%2C+UK!5e0!3m2!1sen!2sin!4v1463669021863" height="462" allowfullscreen></iframe></div>
		
</div><!-- #page -->

		<script>
		( function ( body ) {
			'use strict';
			body.className = body.className.replace( /\btribe-no-js\b/, 'tribe-js' );
		} )( document.body );
		</script>
		<script type="application/ld+json">{"@context":"https:\/\/schema.org\/","@graph":[{"@type":"Product","@id":"https:\/\/order.jamscatering.com.ua\/product\/borsch-chervonii\/","name":"\u0411\u043e\u0440\u0449 \u0447\u0435\u0440\u0432\u043e\u043d\u0438\u0439","url":"https:\/\/order.jamscatering.com.ua\/product\/borsch-chervonii\/"},{"@type":"Product","@id":"https:\/\/order.jamscatering.com.ua\/product\/bulochka\/","name":"\u0411\u0443\u043b\u043e\u0447\u043a\u0430","url":"https:\/\/order.jamscatering.com.ua\/product\/bulochka\/"},{"@type":"Product","@id":"https:\/\/order.jamscatering.com.ua\/product\/bulion-kuryachii\/","name":"\u0411\u0443\u043b\u044c\u0439\u043e\u043d \u043a\u0443\u0440\u044f\u0447\u0438\u0439","url":"https:\/\/order.jamscatering.com.ua\/product\/bulion-kuryachii\/"},{"@type":"Product","@id":"https:\/\/order.jamscatering.com.ua\/product\/burger-z-kuryatinoyu\/","name":"\u0411\u0443\u0440\u0433\u0435\u0440 \u0437 \u043a\u0443\u0440\u044f\u0442\u0438\u043d\u043e\u044e","url":"https:\/\/order.jamscatering.com.ua\/product\/burger-z-kuryatinoyu\/"},{"@type":"Product","@id":"https:\/\/order.jamscatering.com.ua\/product\/burger-z-lososem\/","name":"\u0411\u0443\u0440\u0433\u0435\u0440 \u0437 \u043b\u043e\u0441\u043e\u0441\u0435\u043c","url":"https:\/\/order.jamscatering.com.ua\/product\/burger-z-lososem\/"},{"@type":"Product","@id":"https:\/\/order.jamscatering.com.ua\/product\/burger-z-telyatinoyu\/","name":"\u0411\u0443\u0440\u0433\u0435\u0440 \u0437 \u0442\u0435\u043b\u044f\u0442\u0438\u043d\u043e\u044e","url":"https:\/\/order.jamscatering.com.ua\/product\/burger-z-telyatinoyu\/"},{"@type":"Product","@id":"https:\/\/order.jamscatering.com.ua\/product\/burger-z-shinkoyu\/","name":"\u0411\u0443\u0440\u0433\u0435\u0440 \u0437 \u0448\u0438\u043d\u043a\u043e\u044e","url":"https:\/\/order.jamscatering.com.ua\/product\/burger-z-shinkoyu\/"},{"@type":"Product","@id":"https:\/\/order.jamscatering.com.ua\/product\/burger-z-svininoyu\/","name":"\u0411\u0443\u0440\u0433\u0435\u0440 \u0437\u0456 \u0441\u0432\u0438\u043d\u0438\u043d\u043e\u044e","url":"https:\/\/order.jamscatering.com.ua\/product\/burger-z-svininoyu\/"},{"@type":"Product","@id":"https:\/\/order.jamscatering.com.ua\/product\/buryachkovii-z-kvashenim-og%d1%96rkom\/","name":"\u0411\u0443\u0440\u044f\u0447\u043a\u043e\u0432\u0438\u0439 \u0437 \u043a\u0432\u0430\u0448\u0435\u043d\u0438\u043c \u043e\u0433\u0456\u0440\u043a\u043e\u043c","url":"https:\/\/order.jamscatering.com.ua\/product\/buryachkovii-z-kvashenim-og%d1%96rkom\/"},{"@type":"Product","@id":"https:\/\/order.jamscatering.com.ua\/product\/buryachkovii-z-chornoslivom\/","name":"\u0411\u0443\u0440\u044f\u0447\u043a\u043e\u0432\u0438\u0439 \u0437 \u0447\u043e\u0440\u043d\u043e\u0441\u043b\u0438\u0432\u043e\u043c","url":"https:\/\/order.jamscatering.com.ua\/product\/buryachkovii-z-chornoslivom\/"},{"@type":"Product","@id":"https:\/\/order.jamscatering.com.ua\/product\/v%d1%96dbivna-kuryacha\/","name":"\u0412\u0456\u0434\u0431\u0438\u0432\u043d\u0430-\u043a\u0443\u0440\u044f\u0447\u0430","url":"https:\/\/order.jamscatering.com.ua\/product\/v%d1%96dbivna-kuryacha\/"},{"@type":"Product","@id":"https:\/\/order.jamscatering.com.ua\/product\/v%d1%96dbivna-svinna\/","name":"\u0412\u0456\u0434\u0431\u0438\u0432\u043d\u0430-\u0441\u0432\u0438\u043d\u043d\u0430","url":"https:\/\/order.jamscatering.com.ua\/product\/v%d1%96dbivna-svinna\/"}]}</script><script> /* <![CDATA[ */var tribe_l10n_datatables = {"aria":{"sort_ascending":": activate to sort column ascending","sort_descending":": activate to sort column descending"},"length_menu":"Show _MENU_ entries","empty_table":"No data available in table","info":"Showing _START_ to _END_ of _TOTAL_ entries","info_empty":"Showing 0 to 0 of 0 entries","info_filtered":"(filtered from _MAX_ total entries)","zero_records":"No matching records found","search":"Search:","all_selected_text":"All items on this page were selected. ","select_all_link":"Select all pages","clear_selection":"Clear Selection.","pagination":{"all":"All","next":"Next","previous":"Previous"},"select":{"rows":{"0":"","_":": Selected %d rows","1":": Selected 1 row"}},"datepicker":{"dayNames":["\u041d\u0435\u0434\u0456\u043b\u044f","\u041f\u043e\u043d\u0435\u0434\u0456\u043b\u043e\u043a","\u0412\u0456\u0432\u0442\u043e\u0440\u043e\u043a","\u0421\u0435\u0440\u0435\u0434\u0430","\u0427\u0435\u0442\u0432\u0435\u0440","\u041f\u2019\u044f\u0442\u043d\u0438\u0446\u044f","\u0421\u0443\u0431\u043e\u0442\u0430"],"dayNamesShort":["\u041d\u0434","\u041f\u043d","\u0412\u0442","\u0421\u0440","\u0427\u0442","\u041f\u0442","\u0421\u0431"],"dayNamesMin":["\u041d\u0434","\u041f\u043d","\u0412\u0442","\u0421\u0440","\u0427\u0442","\u041f\u0442","\u0421\u0431"],"monthNames":["\u0421\u0456\u0447\u0435\u043d\u044c","\u041b\u044e\u0442\u0438\u0439","\u0411\u0435\u0440\u0435\u0437\u0435\u043d\u044c","\u041a\u0432\u0456\u0442\u0435\u043d\u044c","\u0422\u0440\u0430\u0432\u0435\u043d\u044c","\u0427\u0435\u0440\u0432\u0435\u043d\u044c","\u041b\u0438\u043f\u0435\u043d\u044c","\u0421\u0435\u0440\u043f\u0435\u043d\u044c","\u0412\u0435\u0440\u0435\u0441\u0435\u043d\u044c","\u0416\u043e\u0432\u0442\u0435\u043d\u044c","\u041b\u0438\u0441\u0442\u043e\u043f\u0430\u0434","\u0413\u0440\u0443\u0434\u0435\u043d\u044c"],"monthNamesShort":["\u0421\u0456\u0447\u0435\u043d\u044c","\u041b\u044e\u0442\u0438\u0439","\u0411\u0435\u0440\u0435\u0437\u0435\u043d\u044c","\u041a\u0432\u0456\u0442\u0435\u043d\u044c","\u0422\u0440\u0430\u0432\u0435\u043d\u044c","\u0427\u0435\u0440\u0432\u0435\u043d\u044c","\u041b\u0438\u043f\u0435\u043d\u044c","\u0421\u0435\u0440\u043f\u0435\u043d\u044c","\u0412\u0435\u0440\u0435\u0441\u0435\u043d\u044c","\u0416\u043e\u0432\u0442\u0435\u043d\u044c","\u041b\u0438\u0441\u0442\u043e\u043f\u0430\u0434","\u0413\u0440\u0443\u0434\u0435\u043d\u044c"],"monthNamesMin":["\u0421\u0456\u0447","\u041b\u044e\u0442","\u0411\u0435\u0440","\u041a\u0432\u0456","\u0422\u0440\u0430","\u0427\u0435\u0440","\u041b\u0438\u043f","\u0421\u0435\u0440","\u0412\u0435\u0440","\u0416\u043e\u0432","\u041b\u0438\u0441","\u0413\u0440\u0443"],"nextText":"\u041d\u0430\u0441\u0442\u0443\u043f\u043d\u0430","prevText":"\u041f\u043e\u043f\u0435\u0440\u0435\u0434\u043d\u044f","currentText":"\u0421\u044c\u043e\u0433\u043e\u0434\u043d\u0456","closeText":"\u0412\u0438\u043a\u043e\u043d\u0430\u043d\u043e","today":"\u0421\u044c\u043e\u0433\u043e\u0434\u043d\u0456","clear":"\u041e\u0447\u0438\u0441\u0442\u0438\u0442\u0438"}};var tribe_system_info = {"sysinfo_optin_nonce":"f30ab92800","clipboard_btn_text":"Copy to clipboard","clipboard_copied_text":"System info copied","clipboard_fail_text":"Press \"Cmd + C\" to copy"};/* ]]> */ </script>	<script type="text/javascript">
		var c = document.body.className;
		c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
		document.body.className = c;
	</script>
	<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-includes/js/admin-bar.min.js?ver=4.9.9'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/order.jamscatering.com.ua\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"\u0411\u0443\u0434\u044c \u043b\u0430\u0441\u043a\u0430, \u043f\u0456\u0434\u0442\u0432\u0435\u0440\u0434\u0456\u0442\u044c, \u0449\u043e \u0432\u0438 \u043d\u0435 \u0440\u043e\u0431\u043e\u0442."}},"cached":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.5'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"\u041f\u0435\u0440\u0435\u0433\u043b\u044f\u043d\u0443\u0442\u0438 \u043a\u043e\u0448\u0438\u043a","cart_url":"https:\/\/order.jamscatering.com.ua\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=3.5.1'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=3.5.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_40e8bc137c9bfe86eb0e5d774c2d6ac4","fragment_name":"wc_fragments_40e8bc137c9bfe86eb0e5d774c2d6ac4"};
/* ]]> */
</script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=3.5.1'></script>
<script type='text/javascript'>
		jQuery( 'body' ).bind( 'wc_fragments_refreshed', function() {
			jQuery( 'body' ).trigger( 'jetpack-lazy-images-load' );
		} );
	
</script>
<script src='https://order.jamscatering.com.ua/wp-content/plugins/the-events-calendar/common/src/resources/js/underscore-before.js'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-includes/js/underscore.min.js?ver=1.8.3'></script>
<script src='https://order.jamscatering.com.ua/wp-content/plugins/the-events-calendar/common/src/resources/js/underscore-after.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpUtilSettings = {"ajax":{"url":"\/wp-admin\/admin-ajax.php"}};
/* ]]> */
</script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-includes/js/wp-util.min.js?ver=4.9.9'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_add_to_cart_variation_params = {"wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_no_matching_variations_text":"\u041d\u0430 \u0436\u0430\u043b\u044c, \u043d\u0435\u043c\u0430\u0454 \u0442\u043e\u0432\u0430\u0440\u0456\u0432, \u044f\u043a\u0456 \u0431 \u0432\u0456\u0434\u043f\u043e\u0432\u0456\u0434\u0430\u043b\u0438 \u0432\u0430\u0448\u043e\u043c\u0443 \u0432\u0438\u0431\u043e\u0440\u0443. \u0411\u0443\u0434\u044c \u043b\u0430\u0441\u043a\u0430, \u0432\u0438\u0431\u0435\u0440\u0456\u0442\u044c \u0456\u043d\u0448\u0435 \u043f\u043e\u0454\u0434\u043d\u0430\u043d\u043d\u044f.","i18n_make_a_selection_text":"\u0411\u0443\u0434\u044c \u043b\u0430\u0441\u043a\u0430 \u043e\u0431\u0435\u0440\u0456\u0442\u044c \u043e\u043f\u0446\u0456\u0457 \u0442\u043e\u0432\u0430\u0440\u0443, \u043f\u0435\u0440\u0448 \u043d\u0456\u0436 \u0434\u043e\u0434\u0430\u0432\u0430\u0442\u0438 \u0446\u0435\u0439 \u0442\u043e\u0432\u0430\u0440 \u0434\u043e \u043a\u043e\u0448\u0438\u043a\u0430.","i18n_unavailable_text":"\u041d\u0430 \u0436\u0430\u043b\u044c, \u0446\u0435\u0439 \u0442\u043e\u0432\u0430\u0440 \u043d\u0435\u0434\u043e\u0441\u0442\u0443\u043f\u043d\u0438\u0439. \u0411\u0443\u0434\u044c \u043b\u0430\u0441\u043a\u0430, \u0432\u0438\u0431\u0435\u0440\u0456\u0442\u044c \u0456\u043d\u0448\u0435 \u043f\u043e\u0454\u0434\u043d\u0430\u043d\u043d\u044f."};
/* ]]> */
</script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart-variation.min.js?ver=3.5.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var yith_wccl_general = {"ajaxurl":"\/?wc-ajax=%%endpoint%%","cart_redirect":"","cart_url":"https:\/\/order.jamscatering.com.ua\/cart\/","view_cart":"View Cart","tooltip":"","tooltip_pos":"","tooltip_ani":"","description":"","add_cart":"","grey_out":"","image_hover":"","wrapper_container_shop":"li.product","image_selector":"img.wp-post-image"};
/* ]]> */
</script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/plugins/yith-woocommerce-product-add-ons/assets/js/yith-wccl.min.js?ver=1.5.12'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/themes/pizzaro/assets/js/navigation.min.js?ver=20120206'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/themes/pizzaro/assets/js/skip-link-focus-fix.min.js?ver=20130115'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/themes/pizzaro/assets/js/bootstrap.min.js?ver=3.3.7'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/themes/pizzaro/assets/js/jquery.waypoints.min.js?ver=4.0.0'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/themes/pizzaro/assets/js/waypoints.sticky.min.js?ver=4.0.0'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/themes/pizzaro/assets/js/readmore.min.js?ver=2.2.0'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/themes/pizzaro/assets/js/jquery.easing.min.js?ver=1.3.2'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/themes/pizzaro/assets/js/scrollup.min.js?ver=1.2.11'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/themes/pizzaro/assets/js/jquery.mCustomScrollbar.concat.min.js?ver=3.1.5'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var pizzaro_options = {"ajax_url":"https:\/\/order.jamscatering.com.ua\/wp-admin\/admin-ajax.php","ajax_loader_url":"https:\/\/order.jamscatering.com.ua\/wp-content\/themes\/pizzaro\/assets\/images\/ajax-loader.gif","enable_sticky_header":"1","enable_excerpt_readmore":"1","excerpt_readmore_data":{"speed":75,"collapsedHeight":50,"moreLink":"<span style=\"display:none\">See More &raquo;<\/span>","lessLink":"<span style=\"display:none\">&laquo; See Less<\/span>"}};
/* ]]> */
</script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/themes/pizzaro/assets/js/scripts.min.js?ver=1.2.11'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-includes/js/jquery/ui/datepicker.min.js?ver=1.11.4'></script>
<script type='text/javascript'>
jQuery(document).ready(function(jQuery){jQuery.datepicker.setDefaults({"closeText":"\u0417\u0430\u043a\u0440\u0438\u0442\u0438","currentText":"\u0421\u044c\u043e\u0433\u043e\u0434\u043d\u0456","monthNames":["\u0421\u0456\u0447\u0435\u043d\u044c","\u041b\u044e\u0442\u0438\u0439","\u0411\u0435\u0440\u0435\u0437\u0435\u043d\u044c","\u041a\u0432\u0456\u0442\u0435\u043d\u044c","\u0422\u0440\u0430\u0432\u0435\u043d\u044c","\u0427\u0435\u0440\u0432\u0435\u043d\u044c","\u041b\u0438\u043f\u0435\u043d\u044c","\u0421\u0435\u0440\u043f\u0435\u043d\u044c","\u0412\u0435\u0440\u0435\u0441\u0435\u043d\u044c","\u0416\u043e\u0432\u0442\u0435\u043d\u044c","\u041b\u0438\u0441\u0442\u043e\u043f\u0430\u0434","\u0413\u0440\u0443\u0434\u0435\u043d\u044c"],"monthNamesShort":["\u0421\u0456\u0447","\u041b\u044e\u0442","\u0411\u0435\u0440","\u041a\u0432\u0456","\u0422\u0440\u0430","\u0427\u0435\u0440","\u041b\u0438\u043f","\u0421\u0435\u0440","\u0412\u0435\u0440","\u0416\u043e\u0432","\u041b\u0438\u0441","\u0413\u0440\u0443"],"nextText":"\u0414\u0430\u043b\u0456","prevText":"\u041f\u043e\u043f\u0435\u0440\u0435\u0434\u043d\u0456\u0439","dayNames":["\u041d\u0435\u0434\u0456\u043b\u044f","\u041f\u043e\u043d\u0435\u0434\u0456\u043b\u043e\u043a","\u0412\u0456\u0432\u0442\u043e\u0440\u043e\u043a","\u0421\u0435\u0440\u0435\u0434\u0430","\u0427\u0435\u0442\u0432\u0435\u0440","\u041f\u2019\u044f\u0442\u043d\u0438\u0446\u044f","\u0421\u0443\u0431\u043e\u0442\u0430"],"dayNamesShort":["\u041d\u0434","\u041f\u043d","\u0412\u0442","\u0421\u0440","\u0427\u0442","\u041f\u0442","\u0421\u0431"],"dayNamesMin":["\u041d\u0434","\u041f\u043d","\u0412\u0442","\u0421\u0440","\u0427\u0442","\u041f\u0442","\u0421\u0431"],"dateFormat":"MM d, yy","firstDay":1,"isRTL":false});});
</script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/plugins/yith-woocommerce-product-add-ons/assets/js/jquery-ui/jquery-ui.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/plugins/yith-woocommerce-product-add-ons/assets/js/accounting.min.js?ver=0.4.2'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/plugins/yith-woocommerce-product-add-ons/assets/js/iris.min.js?ver=1.0.0'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/plugins/yith-woocommerce-product-add-ons/assets/js/color-picker.min.js?ver=1.0.0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var yith_wapo_general = {"ajax_url":"https:\/\/order.jamscatering.com.ua\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","tooltip":"1","tooltip_pos":"top","tooltip_ani":"fade","currency_format_num_decimals":"2","currency_format_symbol":"\u20b4","currency_format_decimal_sep":".","currency_format_thousand_sep":",","currency_format":"%s%v","do_submit":"1","date_format":"mm\/dd\/yy","keep_price_shown":""};
var wpColorPickerL10n = {"clear":"Clear","defaultString":"Default","pick":"Select color","current":"Current color"};
/* ]]> */
</script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/plugins/yith-woocommerce-product-add-ons/assets/js/yith-wapo-frontend.min.js?ver=1.5.12'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/plugins/kingcomposer/assets/frontend/js/kingcomposer.min.js?ver=2.7.6'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-includes/js/wp-embed.min.js?ver=4.9.9'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-includes/js/jquery/ui/widget.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-includes/js/jquery/ui/mouse.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-includes/js/jquery/ui/slider.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/plugins/woocommerce/assets/js/jquery-ui-touch-punch/jquery-ui-touch-punch.min.js?ver=3.5.1'></script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/plugins/woocommerce/assets/js/accounting/accounting.min.js?ver=0.4.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var woocommerce_price_slider_params = {"currency_format_num_decimals":"0","currency_format_symbol":"\u20b4","currency_format_decimal_sep":".","currency_format_thousand_sep":",","currency_format":"%s%v"};
/* ]]> */
</script>
<script type='text/javascript' src='https://order.jamscatering.com.ua/wp-content/plugins/woocommerce/assets/js/frontend/price-slider.min.js?ver=3.5.1'></script>
		<script>	
			jQuery(document).ready(function() {			
				
				if (jQuery('#wp-admin-bar-revslider-default').length>0 && jQuery('.rev_slider_wrapper').length>0) {
					var aliases = new Array();
					jQuery('.rev_slider_wrapper').each(function() {
						aliases.push(jQuery(this).data('alias'));
					});								
					if(aliases.length>0)
						jQuery('#wp-admin-bar-revslider-default li').each(function() {
							var li = jQuery(this),
								t = jQuery.trim(li.find('.ab-item .rs-label').data('alias')); //text()
								
							if (jQuery.inArray(t,aliases)!=-1) {
							} else {
								li.remove();
							}
						});
				} else {
					jQuery('#wp-admin-bar-revslider').remove();
				}
			});
		</script>
			<!--[if lte IE 8]>
		<script type="text/javascript">
			document.body.className = document.body.className.replace( /(^|\s)(no-)?customize-support(?=\s|$)/, '' ) + ' no-customize-support';
		</script>
	<![endif]-->
	<!--[if gte IE 9]><!-->
		<script type="text/javascript">
			(function() {
				var request, b = document.body, c = 'className', cs = 'customize-support', rcs = new RegExp('(^|\\s+)(no-)?'+cs+'(\\s+|$)');

						request = true;
		
				b[c] = b[c].replace( rcs, ' ' );
				// The customizer requires postMessage and CORS (if the site is cross domain)
				b[c] += ( window.postMessage && request ? ' ' : ' no-' ) + cs;
			}());
		</script>
	<!--<![endif]-->
			<div id="wpadminbar" class="nojq nojs">
							<a class="screen-reader-shortcut" href="#wp-toolbar" tabindex="1">Перейти до панелі інструментів</a>
						<div class="quicklinks" id="wp-toolbar" role="navigation" aria-label="Верхня панель" tabindex="0">
				<ul id="wp-admin-bar-root-default" class="ab-top-menu">
		<li id="wp-admin-bar-wp-logo" class="menupop"><a class="ab-item" aria-haspopup="true" href="https://order.jamscatering.com.ua/wp-admin/about.php"><span class="ab-icon"></span><span class="screen-reader-text">Про WordPress</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-wp-logo-default" class="ab-submenu">
		<li id="wp-admin-bar-about"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/about.php">Про WordPress</a>		</li></ul><ul id="wp-admin-bar-wp-logo-external" class="ab-sub-secondary ab-submenu">
		<li id="wp-admin-bar-wporg"><a class="ab-item" href="https://uk.wordpress.org/">WordPress.org</a>		</li>
		<li id="wp-admin-bar-documentation"><a class="ab-item" href="https://codex.wordpress.org/">Документація</a>		</li>
		<li id="wp-admin-bar-support-forums"><a class="ab-item" href="https://uk.wordpress.org/support/">Форум підтримки</a>		</li>
		<li id="wp-admin-bar-feedback"><a class="ab-item" href="https://uk.wordpress.org/support/forum/requests-and-feedback">Зворотній зв'язок</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-site-name" class="menupop"><a class="ab-item" aria-haspopup="true" href="https://order.jamscatering.com.ua/wp-admin/">Jam&#039;s catering</a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-site-name-default" class="ab-submenu">
		<li id="wp-admin-bar-dashboard"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/">Майстерня</a>		</li></ul><ul id="wp-admin-bar-appearance" class="ab-submenu">
		<li id="wp-admin-bar-themes"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/themes.php">Теми</a>		</li>
		<li id="wp-admin-bar-widgets"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/widgets.php">Віджети</a>		</li>
		<li id="wp-admin-bar-menus"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/nav-menus.php">Меню</a>		</li>
		<li id="wp-admin-bar-background" class="hide-if-customize"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/themes.php?page=custom-background">Фон</a>		</li>
		<li id="wp-admin-bar-header" class="hide-if-customize"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/themes.php?page=custom-header">Заголовок</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-customize" class="hide-if-no-customize"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/customize.php?url=https%3A%2F%2Forder.jamscatering.com.ua%2Fshop%2F">Налаштувати</a>		</li>
		<li id="wp-admin-bar-updates"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/update-core.php" title="18 оновлень плагінів, Оновлення перекладів"><span class="ab-icon"></span><span class="ab-label">19</span><span class="screen-reader-text">18 оновлень плагінів, Оновлення перекладів</span></a>		</li>
		<li id="wp-admin-bar-comments"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/edit-comments.php"><span class="ab-icon"></span><span class="ab-label awaiting-mod pending-count count-0" aria-hidden="true">0</span><span class="screen-reader-text">0 коментарів чекають схвалення</span></a>		</li>
		<li id="wp-admin-bar-new-content" class="menupop"><a class="ab-item" aria-haspopup="true" href="https://order.jamscatering.com.ua/wp-admin/post-new.php"><span class="ab-icon"></span><span class="ab-label">Додати</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-new-content-default" class="ab-submenu">
		<li id="wp-admin-bar-new-post"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/post-new.php">Запис</a>		</li>
		<li id="wp-admin-bar-new-media"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/media-new.php">Медіа</a>		</li>
		<li id="wp-admin-bar-new-page"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/post-new.php?post_type=page">Сторінку</a>		</li>
		<li id="wp-admin-bar-new-product"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/post-new.php?post_type=product">Товар</a>		</li>
		<li id="wp-admin-bar-new-shop_order"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/post-new.php?post_type=shop_order">Замовлення</a>		</li>
		<li id="wp-admin-bar-new-static_block"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/post-new.php?post_type=static_block">Static Block</a>		</li>
		<li id="wp-admin-bar-new-feature"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/post-new.php?post_type=feature">Feature</a>		</li>
		<li id="wp-admin-bar-new-wpsl_stores"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/post-new.php?post_type=wpsl_stores">Store</a>		</li>
		<li id="wp-admin-bar-new-tribe_events"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/post-new.php?post_type=tribe_events">Подія</a>		</li>
		<li id="wp-admin-bar-new-kc-section"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/post-new.php?post_type=kc-section">KC Sections</a>		</li>
		<li id="wp-admin-bar-new-user"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/user-new.php">Користувача</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-delete-cache"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/index.php?action=delcachepage&#038;path=%2Fshop%2F&#038;_wpnonce=fec2b3b5f2" title="Delete cache of the current page">Очистити кеш</a>		</li>
		<li id="wp-admin-bar-theme_options"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/admin.php?page=theme_options">Pizzaro</a>		</li>
		<li id="wp-admin-bar-tribe-events" class="menupop"><a class="ab-item" aria-haspopup="true" href="https://order.jamscatering.com.ua/events/"><span class="ab-icon dashicons-before dashicons-calendar"></span>Події</a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-tribe-events-group" class="ab-submenu">
		<li id="wp-admin-bar-tribe-events-view-calendar"><a class="ab-item" href="https://order.jamscatering.com.ua/events/">Перегляд Календаря</a>		</li>
		<li id="wp-admin-bar-tribe-events-add-event"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/post-new.php?post_type=tribe_events">Add Подія</a>		</li>
		<li id="wp-admin-bar-tribe-events-edit-events"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/edit.php?post_type=tribe_events">Редагувати Події</a>		</li></ul><div id="wp-admin-bar-tribe-events-add-ons-group-container" class="ab-group-container"><ul id="wp-admin-bar-tribe-events-import-group" class="ab-submenu">
		<li id="wp-admin-bar-tribe-events-import" class="menupop"><a class="ab-item" aria-haspopup="true" href="https://order.jamscatering.com.ua/wp-admin/edit.php?post_type=tribe_events&#038;page=aggregator">Імпорт</a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-tribe-events-import-default" class="ab-submenu">
		<li id="wp-admin-bar-tribe-aggregator-import-csv"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/edit.php?page=aggregator&#038;post_type=tribe_events&#038;ea-origin=csv">Файл CSV</a>		</li></ul></div>		</li></ul></div><ul id="wp-admin-bar-tribe-events-settings-group" class="ab-submenu">
		<li id="wp-admin-bar-tribe-events-settings"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/edit.php?page=tribe-common&#038;post_type=tribe_events">Налаштування</a>		</li>
		<li id="wp-admin-bar-tribe-events-help"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/edit.php?post_type=tribe_events&#038;page=tribe-help">Допомога</a>		</li>
		<li id="wp-admin-bar-tribe-events-app-shop"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/edit.php?page=tribe-app-shop&#038;post_type=tribe_events">Event Add-Ons</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-revslider" class="menupop revslider-menu"><a class="ab-item" aria-haspopup="true" href="https://order.jamscatering.com.ua/wp-admin/admin.php?page=revslider"><span class="rs-label">Slider Revolution</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-revslider-default" class="ab-submenu">
		<li id="wp-admin-bar-home-v1-slider" class="revslider-sub-menu"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/admin.php?page=revslider&#038;view=slide&#038;id=new&#038;slider=1"><span class="rs-label" data-alias="home-v1-slider">home-v1-slider</span></a>		</li>
		<li id="wp-admin-bar-home-v2-slider" class="revslider-sub-menu"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/admin.php?page=revslider&#038;view=slide&#038;id=new&#038;slider=2"><span class="rs-label" data-alias="home-v2-slider">home-v2-slider</span></a>		</li>
		<li id="wp-admin-bar-home-v6-slider" class="revslider-sub-menu"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/admin.php?page=revslider&#038;view=slide&#038;id=new&#038;slider=3"><span class="rs-label" data-alias="home-v6-slider">home-v6-slider</span></a>		</li>
		<li id="wp-admin-bar-home-v7-slider" class="revslider-sub-menu"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/admin.php?page=revslider&#038;view=slide&#038;id=new&#038;slider=4"><span class="rs-label" data-alias="home-v7-slider">home-v7-slider</span></a>		</li></ul></div>		</li></ul><ul id="wp-admin-bar-top-secondary" class="ab-top-secondary ab-top-menu">
		<li id="wp-admin-bar-search" class="admin-bar-search"><div class="ab-item ab-empty-item" tabindex="-1"><form action="https://order.jamscatering.com.ua/" method="get" id="adminbarsearch"><input class="adminbar-input" name="s" id="adminbar-search" type="text" value="" maxlength="150" /><label for="adminbar-search" class="screen-reader-text">Пошук</label><input type="submit" class="adminbar-button" value="Пошук"/></form></div>		</li>
		<li id="wp-admin-bar-my-account" class="menupop with-avatar"><a class="ab-item" aria-haspopup="true" href="https://order.jamscatering.com.ua/wp-admin/profile.php">Привіт, <span class="display-name">admin</span><img alt='' src='https://secure.gravatar.com/avatar/072132da38b1ba0f956a8383062b8ee4?s=26&#038;d=mm&#038;r=g' srcset='https://secure.gravatar.com/avatar/072132da38b1ba0f956a8383062b8ee4?s=52&#038;d=mm&#038;r=g 2x' class='avatar avatar-26 photo' height='26' width='26' /></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-user-actions" class="ab-submenu">
		<li id="wp-admin-bar-user-info"><a class="ab-item" tabindex="-1" href="https://order.jamscatering.com.ua/wp-admin/profile.php"><img alt='' src='https://secure.gravatar.com/avatar/072132da38b1ba0f956a8383062b8ee4?s=64&#038;d=mm&#038;r=g' srcset='https://secure.gravatar.com/avatar/072132da38b1ba0f956a8383062b8ee4?s=128&#038;d=mm&#038;r=g 2x' class='avatar avatar-64 photo' height='64' width='64' /><span class='display-name'>admin</span></a>		</li>
		<li id="wp-admin-bar-edit-profile"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-admin/profile.php">Редагувати мій обліковий запис</a>		</li>
		<li id="wp-admin-bar-logout"><a class="ab-item" href="https://order.jamscatering.com.ua/wp-login.php?action=logout&#038;_wpnonce=fc3d585581">Вийти</a>		</li></ul></div>		</li></ul>			</div>
						<a class="screen-reader-shortcut" href="https://order.jamscatering.com.ua/wp-login.php?action=logout&#038;_wpnonce=fc3d585581">Вийти</a>
					</div>

		
</body>
</html>

<!-- Dynamic page generated in 2.717 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2019-10-11 11:50:41 -->
