<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package pizzaro
 */

?>

		</div><!-- .col-full -->
	</div><!-- #content -->

	<?php do_action( 'pizzaro_before_footer_v1' ); ?>

<script type="text/javascript">
	$ = jQuery.noConflict();
	var href = document.URL;
	var hrefall = document.location.host;
	function get_products(e){
		$.post({
	        url: 'https://order.jamscatering.com.ua/wp-content/plugins/product-filter/includes/scripts/check_available_products.php',
	        data: {
	            /*order_date: date,*/
	            url: href
	        },
	        success: function (data) {
	            $('ul.products').html('');
	            $('ul.products').append(data);
	            $('ul.page-numbers').css('display', 'none');
	            //sessionStorage.setItem('order_date', date)
	            $('.modal').remove();		
	        }
	    });
	}

	get_products();
</script>
<?/*<script src="<?=plugins_url().'/product-filter/includes/scripts/product_filter.js'?>"></script>*/?>
	<footer id="colophon" class="site-footer footer-v1" role="contentinfo">
		<div class="col-full">

			<?php
			/**
			 * Functions hooked in to pizzaro_footer action
			 *
			 * @hooked pizzaro_footer_widgets - 10
			 * @hooked pizzaro_credit         - 20
			 */
			do_action( 'pizzaro_footer_v1' ); ?>

		</div><!-- .col-full -->
	</footer><!-- #colophon -->

	<?php do_action( 'pizzaro_after_footer_v1' ); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>