<?php
/**
 * Pizzaro Child
 *
 * @package pizzaro-child
 */
/**
 * Include all your custom code here
 */



add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
add_filter('wc_session_expiring', 'so_26545001_filter_session_expiring' );

function so_26545001_filter_session_expiring($seconds) {
    return 60 * 60 * 2; // 2 hours
}

add_filter('wc_session_expiration', 'so_26545001_filter_session_expired' );

function so_26545001_filter_session_expired($seconds) {
    return 60 * 60 * 3; // 3 hours
}
function custom_override_checkout_fields( $fields ) {
  //unset($fields['billing']['billing_first_name']);// имя
  //unset($fields['billing']['billing_last_name']);// фамилия
  //unset($fields['billing']['billing_company']); // компания
  //unset($fields['billing']['billing_address_1']);//
  //unset($fields['billing']['billing_address_2']);//
  //unset($fields['billing']['billing_city']);
  unset($fields['billing']['billing_postcode']);
  unset($fields['billing']['billing_country']);
  unset($fields['billing']['billing_state']);
  //unset($fields['billing']['billing_phone']);
  //unset($fields['order']['order_comments']);
  //unset($fields['billing']['billing_email']);
  //unset($fields['account']['account_username']);
  //unset($fields['account']['account_password']);
  //unset($fields['account']['account_password-2']);

 
  unset($fields['billing']['billing_postcode']);// индекс 
    return $fields;
}
add_filter('posts_clauses', 'order_by_stock_status');
function order_by_stock_status($posts_clauses) {
    global $wpdb;
    // only change query on WooCommerce loops
    if (is_woocommerce() && (is_shop() || is_product_category() || is_product_tag())) {
        $posts_clauses['join'] .= " INNER JOIN $wpdb->postmeta istockstatus ON ($wpdb->posts.ID = istockstatus.post_id) ";
        $posts_clauses['orderby'] = " istockstatus.meta_value ASC, " . $posts_clauses['orderby'];
        $posts_clauses['where'] = " AND istockstatus.meta_key = '_stock_status' AND istockstatus.meta_value <> '' " . $posts_clauses['where'];
    }
    return $posts_clauses;
}


function wc_account_menu_items($items) {
     unset($items['dashboard']);         // убрать вкладку Консоль
    // unset($items['orders']);             // убрать вкладку Заказы
     unset($items['downloads']);         // убрать вкладку Загрузки
    // unset($items['edit-address']);         // убрать вкладку Адреса
    // unset($items['edit-account']);         // убрать вкладку Детали учетной записи
    // unset($items['customer-logout']);     // убрать вкладку Выйти
    return $items;
}
 
add_filter( 'woocommerce_account_menu_items', 'wc_account_menu_items', 10 );



function my_template_loop_product_title(){
    global $product;
    echo '<h3 itemprop="name" class="product_title entry-title">Вага: ';
    $versionvalues = get_the_terms( $product->id, 'pa_220');

    if (is_array($versionvalues)) {
        foreach ( $versionvalues as $versionvalue ) {
            echo $versionvalue->name;
        }
    }

    echo '</h3>';
}
add_action( 'woocommerce_shop_loop_item_title', 'my_template_loop_product_title', 10 );