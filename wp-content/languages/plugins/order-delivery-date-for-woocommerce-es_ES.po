# Translation of Plugins - Order Delivery Date for WooCommerce - Development (trunk) in Spanish (Spain)
# This file is distributed under the same license as the Plugins - Order Delivery Date for WooCommerce - Development (trunk) package.
msgid ""
msgstr ""
"PO-Revision-Date: 2018-10-07 20:09:18+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: GlotPress/2.4.0-alpha\n"
"Language: es\n"
"Project-Id-Version: Plugins - Order Delivery Date for WooCommerce - Development (trunk)\n"

#: welcome.php:239
msgid "Current Offers"
msgstr "Ofertas actuales"

#: welcome.php:222
msgid "View all Premium Plugins"
msgstr "Ver todos los plugins premium"

#: welcome.php:221
msgid "Visit the Tyche Softwares Website"
msgstr "Visita el sitio web de Tyche Softwares"

#: welcome.php:218
msgid "Getting to Know Tyche Softwares"
msgstr "Conociendo a Tyche Softwares"

#: welcome.php:231
msgid "Meet the team"
msgstr "Conoce a nuestro equipo"

#: welcome.php:181
msgid "The ability to synchronise deliveries to the google calendar helps administrator or store manager to manage all the things in a single calendar."
msgstr "La capacidad de sincronizar envíos con Google Calendar ayuda al administrador o encargado de tienda a gestionarlo todo desde un mismo calendario."

#: welcome.php:179
msgid "Synchronise Deliveries with Google Calendar"
msgstr "Sincronizar envíos con Google Calendar"

#: welcome.php:144
msgid "Create Custom Delivery Schedules"
msgstr "Crear órdenes de entrega personalizadas"

#: welcome.php:146
msgid "The ability to set different delivery schedule for different WooCommerce shipping zones, shipping classes and product categories is very useful for the businesses like food packet deliveries, cake shops etc which deals with delivery in different shipping zones."
msgstr "La capacidad de establecer diferentes órdenes de entrega para diferentes zonas de envío, tipos de envío y categorías de productos de WooCommerce es muy útil para comercios como los de envío de paquetes de alimentos, pastelerías, etc. que distribuyen en diferentes zonas de envío."

#: welcome.php:168 welcome.php:175 welcome.php:204
msgid "Order Delivery Date for WooCommerce Lite"
msgstr "Order Delivery Date para WooCommerce Lite"

#: welcome.php:123
msgid "Click Here to go to Order Delivery Date Settings page"
msgstr "Haz clic aquí para ir a la página de ajustes de Order Delivery Date"

#: welcome.php:121
msgid "To start allowing customers to select their preferred delivery date, simply activate the Enable Delivery Date checkbox from under Order Delivery Date menu."
msgstr "Para empezar a permitir a los clientes seleccionar su fecha de entrega preferida, simplemente activa la casilla de verificación \"Habilitar fecha de entrega\", debajo del menú \"Order Delivery Date\"."

#: welcome.php:119
msgid "Enable Delivery Date Capture"
msgstr "Activar la opción de fecha de entrega"

#: welcome.php:115 welcome.php:140
msgid "Order Delivery Date Lite"
msgstr "Order Delivery Date Lite"

#: welcome.php:111
msgid "Get Started with Order Delivery Date Lite"
msgstr "Empieza a usar Order Delivery Date Lite"

#: welcome.php:57
msgid "Welcome to Order Delivery Date Lite"
msgstr "Bienvenido a Order Delivery Date Lite"

#: welcome.php:56
msgid "Welcome to Order Delivery Date Lite %s"
msgstr "Bienvenido a Order Delivery Date Lite %s"

#: welcome.php:195
msgid "The Pro version of the plugin allows you to add different delivery settings like Same day cut-off time, Next Day cut-off time or Minimum Delivery Time for each weekday. It also allows you to add different delivery charges for different weekdays."
msgstr "La versión Pro del plugin te permite añadir diferentes configuraciones de envío como \"Entrega el mismo día\", \"Entrega al día siguiente\" o \"Plazo mínimo de entrega\" para cada día de la semana. También te permite añadir diferentes gastos de envío para diferentes días de la semana."

#: welcome.php:209
msgid "View full list of differences between Lite & Pro plugin"
msgstr "Ver la lista completa de diferencias entre las versiones Lite y Pro del plugin"

#: welcome.php:160
msgid "The provision for allowing Delivery Time along with the Delivery Date on the checkout page makes the delivery more accurate. Delivering products on customer's preferred date and time improves your customers service."
msgstr "La prestación que permite escoger la Hora de entrega junto a la Fecha de entrega en la página de pago hace que el envío sea más acurado. Entregar los productos en la fecha y hora preferidos por el cliente mejora su experiencia de compra."

#: welcome.php:158
msgid "Delivery Time along with Delivery Date"
msgstr "La Hora de entrega junto a La Fecha de entrega"

#: welcome.php:149 welcome.php:162 welcome.php:184 welcome.php:198
msgid "Learn More"
msgstr "Aprender más"

#: welcome.php:135
msgid "The Order Delivery Date Pro plugin gives you features where you can allow customers to choose a Delivery Time along with Date as compared to Lite Plugin. Here are some other notable features the Pro version provides."
msgstr ""
"La versión Pro del plugin Order Delivery Date te ofrece características con las que puedes permitir a los clientes escoger una Hora de entrega junto a la Fecha de entrega, en comparación con la versión Lite del plugin.\n"
"Aquí hay otras características notables que la versión Pro ofrece."

#: welcome.php:133
msgid "Know more about Order Delivery Date Pro"
msgstr "Saber más acerca de Order Delivery Date Pro"

#: welcome.php:101
msgid "Thank you for activating or updating to the latest version of Order Delivery Date Lite! If you're a first time user, welcome! You're well to accept deliveries with customer preferred delivery date. "
msgstr "¡Gracias por activar o actualizar a la última versión de Order Delivery Date Lite! Si eres un nuevo usuario, ¡bienvenido! A partir de ahora vas a poder aceptar envíos con la fecha de entrega preferida por el cliente. "

#: welcome.php:193
msgid "Different delivery settings for each weekday"
msgstr "Ajustes de envío diferentes para cada día de la semana"

#: welcome.php:286
msgid "Follow %s"
msgstr "Seguir a %s"

#: order_delivery_date.php:375
msgid "Booked"
msgstr "Reservado"

#: order_delivery_date.php:374
msgid "Holiday"
msgstr "Día festivo"

#: orddd-lite-settings.php:340
msgid "Date:"
msgstr "Fecha:"

#: orddd-lite-settings.php:331
msgid "Name:"
msgstr "Nombre:"

#: orddd-lite-settings.php:324
msgid "Add Holiday"
msgstr "Añadir día festivo"

#: orddd-lite-process.php:284
msgid "is a required field."
msgstr "es un campo requerido."

#: orddd-lite-settings.php:233
msgid "Add the Delivery Date field on the cart page along with the Checkout page."
msgstr "Añadir el campo \"Fecha de entrega\" en la página de Carrito de compra y en la página de Finalizar compra."

#: orddd-lite-settings.php:229
msgid "Delivery Date field on Cart page:"
msgstr "Campo \"Fecha de entrega\" en la página de Carrito de compra:"

#. Author URI of the plugin/theme
msgid "http://www.tychesoftwares.com/about"
msgstr "http://www.tychesoftwares.com/about"

#. Author of the plugin/theme
msgid "Tyche Softwares"
msgstr "Tyche Softwares"

#. Description of the plugin/theme
msgid "This plugin allows customers to choose their preferred Order Delivery Date during checkout."
msgstr "Este plugin permite a los clientes escoger su fecha de entrega preferida al finalizar la compra."

#. Plugin URI of the plugin/theme
msgid "http://www.tychesoftwares.com/store/free-plugin/order-delivery-date-on-checkout/"
msgstr "http://www.tychesoftwares.com/store/free-plugin/order-delivery-date-on-checkout/"

#. Plugin Name of the plugin/theme
msgid "Order Delivery Date for WooCommerce (Lite version)"
msgstr "Order Delivery Date para WooCommerce (versión Lite)"

#: order_delivery_date.php:373
msgid "Clear"
msgstr "Vaciar"

#: order_delivery_date.php:192
msgid "Order Delivery Date for WooCommerce (Lite version) plugin requires WooCommerce installed and activate."
msgstr "El plugin Order Delivery Date para WooCommerce (versión Lite) requiere tener WooCommerce instalado y activado."

#: orddd-lite-settings.php:787
msgid "Featured Products"
msgstr "Productos destacados"

#: orddd-lite-settings.php:779
msgid "Virtual Products"
msgstr "Productos virtuales"

#: orddd-lite-settings.php:765
msgid "Preview theme:"
msgstr "Previsualizar tema:"

#: orddd-lite-settings.php:692
msgid "In Shipping Section"
msgstr "En el departamento de envíos"

#: orddd-lite-settings.php:691
msgid "In Billing Section"
msgstr "En el departamento de facturación"

#: orddd-lite-settings.php:392 orddd-lite-settings.php:400
#: orddd-lite-settings.php:408
msgid "Save Settings"
msgstr "Guardar ajustes"

#: orddd-lite-settings.php:384 orddd-lite-settings.php:412
msgid "Holidays"
msgstr "Festivos"

#: orddd-lite-settings.php:443
msgid "Weekdays:"
msgstr "Días laborables:"

#: orddd-lite-settings.php:383
msgid "Appearance"
msgstr "Apariencia"

#: orddd-lite-settings.php:247
msgid "Disable the Delivery Date Field for:"
msgstr "Desactivar el campo \"Fecha de entrega\" para:"

#: orddd-lite-settings.php:242
msgid "Select the theme for the calendar which blends with the design of your website."
msgstr "Selecciona el tema para el calendario que combine mejor con el diseño de tu sitio web."

#: orddd-lite-settings.php:238
msgid "Theme:"
msgstr "Tema:"

#: orddd-lite-settings.php:224
msgid "</br>The Delivery Date field will be displayed in the selected section.</br><i>Note: WooCommerce automatically hides the Shipping section fields for Virtual products.</i>"
msgstr "</br>El campo \"Fecha de entrega\" aparecerá en la sección seleccionada.</br><i>Nota: WooCommerce esconde automáticamente los campos de envío para productos virtuales.</i>"

#: orddd-lite-settings.php:220
msgid "Field placement on the Checkout page:"
msgstr "Ubicación del campo en la página de Finalizar compra:"

#: orddd-lite-settings.php:251
msgid "<br>Disable the Delivery Date on the Checkout page for Virtual products and Featured products."
msgstr "<br>Desactivar la Fecha de entrega en la página de Finalizar compra para Productos virtuales y Productos destacados. "

#: orddd-lite-settings.php:215
msgid "The number of months to be shown on the calendar."
msgstr "Número de meses que se muestran en el calendario."

#: orddd-lite-settings.php:211
msgid "Number of Months:"
msgstr "Número de meses:"

#: orddd-lite-settings.php:206
msgid "Choose the note to be displayed below the delivery date field on checkout page."
msgstr "Escoge la nota que se mostrará debajo del campo de fecha de entrega en la página de finalizar compra."

#: orddd-lite-settings.php:202
msgid "Field Note Text:"
msgstr "Texto en el campo \"Nota\":"

#: orddd-lite-settings.php:197
msgid "Choose the placeholder text that is to be displayed for the field on checkout page."
msgstr "Escoge el texto del marcador de posición que se mostrará para el campo en la página de Finalizar compra."

#: orddd-lite-settings.php:193
msgid "Field Placeholder Text:"
msgstr "Texto en el campo \"Marcador de posición\":"

#: orddd-lite-settings.php:188
msgid "Choose the label that is to be displayed for the field on checkout page."
msgstr "Escoge la etiqueta que se mostrará para el campo en la página de finalizar compra."

#: orddd-lite-settings.php:179
msgid "Choose the first day of week displayed on the Delivery Date calendar."
msgstr "Escoge el primer día de la semana que se mostrará en el calendario de fecha de entrega."

#: orddd-lite-settings.php:175
msgid "First Day of Week:"
msgstr "Primer día de la semana:"

#: orddd-lite-settings.php:170
msgid "The format in which the Delivery Date appears to the customers on the checkout page once the date is selected."
msgstr "El formato en el que la fecha de entrega se muestra a los clientes en la página de finalizar compra una vez que la fecha se ha seleccionado."

#: orddd-lite-settings.php:166
msgid "Date Format:"
msgstr "Formato de fecha:"

#: orddd-lite-settings.php:157
msgid "Calendar Language:"
msgstr "Idioma del calendario:"

#: orddd-lite-settings.php:150
msgid "Calendar Appearance"
msgstr "Apariencia del calendario"

#: orddd-lite-settings.php:87
msgid "Auto-populate first available Delivery date when the checkout page loads."
msgstr "Rellenar automáticamente con la primera fecha de entrega disponible cuando se cargue la página de finalizar compra."

#: orddd-lite-settings.php:83
msgid "Auto-populate first available Delivery date:"
msgstr "Rellenar automáticamente con la primera fecha de entrega disponible:"

#: orddd-lite-settings.php:78
msgid "Enable default sorting of orders (in descending order) by Delivery Date on WooCommerce -> Orders page"
msgstr "Activar la ordenación por defecto de los pedidos (en orden descendiente) por fecha de entrega en WooCommerce -> Página de pedidos"

#: orddd-lite-settings.php:74
msgid "Sort on WooCommerce Orders Page:"
msgstr "Ordenar en la página de pedidos de WooCommerce:"

#: orddd-lite-settings.php:69
msgid "Maximum deliveries/orders per day."
msgstr "Número máximo de entregas/pedidos por día."

#: orddd-lite-settings.php:60
msgid "Selection of delivery date on the checkout page will become mandatory."
msgstr "La selección de fecha de entrega en la página de finalizar compra será obligatoria."

#: orddd-lite-settings.php:47
msgid "Number of dates to choose:"
msgstr "Número de fechas para escoger:"

#: orddd-lite-settings.php:42
msgid "Minimum number of hours required to prepare for delivery."
msgstr "Número mínimo de horas requerido para preparar el envío."

#: orddd-lite-settings.php:382
msgid "Date Settings"
msgstr "Ajustes de fecha"

#: orddd-lite-settings.php:184
msgid "Field Label:"
msgstr "Etiqueta del campo:"

#: orddd-lite-settings.php:161
msgid "Choose a Language."
msgstr "Escoge un idioma."

#: orddd-lite-settings.php:96
msgid "If selected, then the Minimum Delivery Time (in hours) will be applied on the non working weekdays which are unchecked in Delivery Weekdays. If unchecked, then it will not be applied. For example, if Minimum Delivery Time (in hours) is set to 48 hours and Saturday is disabled for delivery. Now if a customer visits the website on Firday, then the first available date will be Monday and not Sunday."
msgstr "Si está seleccionado, el \"Plazo mínimo de entrega\" (en horas) será aplicado a los días no laborables que estén desmarcados en \"Días de entrega\". Si no está seleccionado, entonces no se aplicará. Por ejemplo, si el plazo mínimo de entrega (en horas) está establecido en 48 horas y el sábado está desactivado para la entrega. Entonces si un cliente visita el sitio web un viernes, la primera fecha de entrega disponible será lunes y no domingo."

#: orddd-lite-settings.php:92
msgid "Apply Minimum Delivery Time for non working weekdays:"
msgstr "Aplicar un plazo mínimo de entrega para los días no laborables:"

#: orddd-lite-settings.php:65
msgid "Lockout date after X orders:"
msgstr "Bloquear la fecha después de X pedidos:"

#: orddd-lite-settings.php:51
msgid "Number of dates available for delivery."
msgstr "Número de fechas disponibles para entrega."

#: orddd-lite-settings.php:694
msgid "After Order Notes"
msgstr "Notas para después del pedido"

#: orddd-lite-settings.php:693
msgid "Before Order Notes"
msgstr "Notas para antes del pedido"

#: orddd-lite-settings.php:56
msgid "Mandatory field?:"
msgstr "¿Campo obligatorio?:"

#: orddd-lite-settings.php:38
msgid "Minimum Delivery time (in hours):"
msgstr "Plazo mínimo de entrega (en horas):"

#: orddd-lite-settings.php:29
msgid "Delivery Days:"
msgstr "Días de entrega:"

#: orddd-lite-settings.php:13 orddd-lite-settings.php:377
msgid "Order Delivery Date Settings"
msgstr "Ajustes de Order Delivery Date"

#: orddd-lite-config.php:126 uninstall.php:27
msgid "Saturday"
msgstr "Sábado"

#: orddd-lite-config.php:125 uninstall.php:26
msgid "Friday"
msgstr "Viernes"

#: orddd-lite-config.php:124 uninstall.php:25
msgid "Thursday"
msgstr "Jueves"

#: orddd-lite-config.php:123 uninstall.php:24
msgid "Wednesday"
msgstr "Miércoles"

#: orddd-lite-config.php:122 uninstall.php:23
msgid "Tuesday"
msgstr "Martes"

#: orddd-lite-config.php:121 uninstall.php:22
msgid "Monday"
msgstr "Lunes"

#: orddd-lite-config.php:120 uninstall.php:21
msgid "Sunday"
msgstr "Domingo"

#: class-view-holidays.php:83
msgid "Date"
msgstr "Fecha"

#: class-view-holidays.php:82
msgid "Name"
msgstr "Nombre"

#: class-view-holidays.php:42
msgid "Delete"
msgstr "Borrar"

#: class-view-holidays.php:33
msgid "holidays"
msgstr "festivos"

#: class-view-holidays.php:32
msgid "holiday"
msgstr "festivo"

#: orddd-lite-settings.php:33
msgid "Select weekdays for delivery."
msgstr "Seleccionar los días de la semana para la entrega."

#: orddd-lite-settings.php:24
msgid "Enable Delivery Date capture on the checkout page."
msgstr "Activar la opción \"Fecha de entrega\" en la página de finalizar compra."

#: orddd-lite-settings.php:20
msgid "Enable Delivery Date:"
msgstr "Activar \"Fecha de entrega\":"