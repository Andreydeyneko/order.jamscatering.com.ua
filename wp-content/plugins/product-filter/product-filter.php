<?php

/*
Plugin Name: Product filter by date
Description: Этот плагин позволяет добавлять товары в список по дням для доступности
Author: dante6674
*/

require_once plugin_dir_path(__FILE__) . 'includes/pf-functions.php';

register_activation_hook(__FILE__,'add_database_table');
register_deactivation_hook(__FILE__, 'delete_table');

function add_database_table()
{
    global $wpdb;
    $tableName = $wpdb->prefix.'filtered_products';

    if ($wpdb->get_var("SHOW TABLES LIKE '$tableName'") != $tableName) {
        $sql = "CREATE TABLE " . $tableName . " (
	  id mediumint(9) NOT NULL AUTO_INCREMENT,
	  product_id bigint(11) DEFAULT '0' NOT NULL,
	  day_name varchar(255) NOT NULL,
	  UNIQUE KEY id (id)
	);";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}

function delete_table()
{
    global $wpdb;
    $tableName = $wpdb->prefix.'filtered_products';

    if ($wpdb->get_var("SHOW TABLES LIKE '$tableName'") != $tableName) {
        $sql = "DROP TABLE IF EXISTS '$tableName' CASCADE;";
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}

// wp_enqueue_script('product_filter', '/jamscatering.com.ua/order/wp-content/plugins/product-filter/includes/scripts/');
/*wp_enqueue_script('product_filter', plugin_dir_url( __FILE__ ) . 'includes/scripts/');
wp_localize_script('product_filter', 'productFilter', array(
    'pluginsUrl' => plugins_url()
));*/