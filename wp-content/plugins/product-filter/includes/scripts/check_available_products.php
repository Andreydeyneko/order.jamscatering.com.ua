<?php
// require('/home/jamscate/jamscatering.com.ua/order/wp-load.php');

require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );

global $wpdb;
$url = explode('/',parse_url($_POST['url'])['path'])['2'];

/*
$currentHour =  date('H');
$currentDate = date('Y-m-d');
$orderDate = $_POST['order_date'];
if ($currentHour >= 16
    && date('Y-m-d', strtotime($currentDate .' +1 day')) === $orderDate) {
    echo <<<HTML
    <h1>Ви не можете зробити замовлення на наступний день після 18:00</h1>
HTML;
exit();
}

if ($currentDate === $orderDate ) {
    echo <<<HTML
    <h1>Ви не можете зробити замовлення на поточний день</h1>
HTML;
    exit();
}

if (strtotime($orderDate) < time()) {
    echo <<<HTML
    <h1>Ви не можете зробити замовлення на минулi днi</h1>
HTML;
    exit();
}

$_SESSION['order_date'] = $orderDate;*/


$dayName = date('l', strtotime($_POST['order_date']));

//$availableIds = $wpdb->get_results("select product_id from wp_filtered_products where day_name = '$dayName'");
$availableIds = $wpdb->get_results("select product_id from wp_filtered_products");
//var_dump($availableIds);

foreach ($availableIds as $key =>  $availableId) {
    $availableIdstemp[] = intval($availableId->product_id);
}

$availableIds = array_unique($availableIdstemp);


$catID = $wpdb->get_results("select term_id from wp_terms where slug = '$url'");

$allowEcho = true;
if ($url != '') {
    $allowEcho = false;
}
    $counter = 1;
    /*сортировка*/
foreach ($availableIds as $availableId) {
    $product = wc_get_product($availableId);    
    $categories = $product->get_category_ids();
    if(in_array(66, $categories)){
        $first[] = $availableId; 
    }else{
        $else[] = $availableId;
    }
}

foreach ($else as $key => $value) {
    array_push($first, $value);
}
/*сортировка конец*/
foreach ($first as $availableId) {

    $position = 'first';
    if ($counter == 2) {
        $position = 'second';
    } elseif ($counter == 3) {
        $position = 'third';
        $counter =0;
    }
    $product = wc_get_product($availableId);
    $price = $product->get_price();
    $thumb = $product->get_image();
    $title = $product->get_title();
    $urlCart = $product->add_to_cart_url();
    $id = $product->get_id();
    $link = $product->get_permalink();
    $categories = $product->get_category_ids();
    $weight = $product->get_attribute('pa_220') ? $product->get_attribute('pa_220') : '';
    $kal = $product->get_attribute('pa_kal') ? $product->get_attribute('pa_kal') : '';
    $sklad = $product->get_attribute('pa_sklad') ? $product->get_attribute('pa_sklad') : '';

    if (in_array($catID[0]->term_id, $categories) || $allowEcho) {
        echo <<<HTML
    <li class="post-$id product type-product status-publish has-post-thumbnail product_cat-fast-food $position shipping-taxable purchasable product-type-simple addon-product instock">
    <div class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
    <div class="product-outer" style="display: block!important; height: 383px;"><div class="product-inner">     <div class="product-image-wrapper">
                $thumb
                <div class="product-content-wrapper">
        <h2 class="woocommerce-loop-product__title">$title</h2>
HTML;
    if ($weight !== '') {
        echo <<<HTML
        <h3 itemprop="name" class="product_title entry-title">Вага: $weight</h3>
HTML;
    }
    echo <<<HTML
 <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">₴</span>$price</span></span>
<div class="hover-area">
<a href="/?add-to-cart=$id&quantity=1" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="$id" data-product_sku="" aria-label="Додайте “$title” до кошика" rel="nofollow">Додати до кошика</a>
HTML;

if ($kal !== '') {
        echo <<<HTML
        <div class="product-kal"><strong>Калорійність:</strong> $kal кКал</div>
HTML;
    }

if ($sklad !== '') {
        echo <<<HTML
        <div class="product-skl"><strong>Склад:</strong> $sklad</div>
HTML;
    }

echo <<<HTML
</div>      </div>
        </div><!-- /.product-inner --></div><!-- /.product-outer --></div></li>
HTML;
        $counter++;
    }
}