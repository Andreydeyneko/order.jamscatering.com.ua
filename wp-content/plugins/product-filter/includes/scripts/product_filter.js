
$ = jQuery.noConflict();

var href = document.URL;
var hrefall = document.location.host;

if (sessionStorage.getItem('order_date') === null
    && href.match(hrefall+'/shop')
    || href.match(hrefall+'/product-category')
    && sessionStorage.getItem('order_date') === null) {
    $('.content-area').append('<div class="modal fade" id="modalOrderDateForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"\n' +
        '  aria-hidden="false" style="display: block">\n' +
        '  <div class="modal-dialog" role="document">\n' +
        '    <div class="modal-content">\n' +
        '      <div class="modal-header text-center">\n' +
        '        <h4 class="modal-title w-100 font-weight-bold">Виберіть дату замовлення</h4>\n' +
        '        <button type="button" class="close" data-dismiss="modal" aria-label="Close">\n' +
        '          <span aria-hidden="false">&times;</span>\n' +
        '        </button>\n' +
        '      </div>\n' +
        '        <div class="md-form mb-4">' +
        '      <input type="date" class="date-picker form-control" name="order_date" id="order-date">' +
        '      ' +
        '<button id="submit-order-date">Підтвердити</button>'+
        '</div>\n' +
        '    </div>\n' +
        '  </div>\n' +
        '</div>');
} else {
    this.deactivateProductsIfByDate(sessionStorage.getItem('order_date'));
}

$('#modalOrderDateForm').removeClass('fade');
$('#submit-order-date').click(function () {
    var date = $('#order-date').val();
    deactivateProductsIfByDate(date);
});

function deactivateProductsIfByDate(date)
{
    $.post({
        url: productFilter.pluginsUrl + '/product-filter/includes/scripts/check_available_products.php',
        data: {
            order_date: date,
            url: href
        },
        success: function (data) {
            $('ul.products').html('');
            $('ul.products').append(data);
            $('ul.page-numbers').css('display', 'none');
            sessionStorage.setItem('order_date', date)
            $('.modal').remove();
if (sessionStorage.getItem('order_date') !== null
    && href.match(hrefall+'/shop')
    || href.match(hrefall+'/product-category')
    && sessionStorage.getItem('order_date') !== null) {
                $('#change-order-date').css('display', 'block');
            }
        }
    });
}

$('#change-order-date').click(function () {
    sessionStorage.removeItem('order_date');
    location.reload();
});