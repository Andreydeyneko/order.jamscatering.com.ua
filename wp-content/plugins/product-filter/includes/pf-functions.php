<?php

add_action('admin_menu', 'add_menu_item');
add_action('add_to_filter', 'insert_to_filter');
function add_menu_item()
{
    add_menu_page(
        'Product Filter',
        'Product Filter',
        'manage_options',
        'product-filter/includes/pages/pf-acp-page.php'
    );
}

function insert_to_filter($product_id, $day_name)
{
    global $wpdb;
    $tableName = $wpdb->prefix.'filtered_products';

    $wpdb->insert($tableName, [
        'product_id' => $_POST['product_id'],
        'day_name' => $_POST['day_name']
    ]);
}