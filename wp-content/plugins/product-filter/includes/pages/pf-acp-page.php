<?php

$products = wc_get_products(array(
    'return' => 'names',
    'limit' => 1000000
));

global $wpdb;
$tableName = $wpdb->prefix.'filtered_products';
$filters = $wpdb->get_results('select * from wp_filtered_products');
?>

<div class="wrap">
   <h1>Product Filter</h1>

    <form action="<?=plugins_url('/product-filter/includes/scripts/add_product_to_date_filter.php')?>" method="post">
        <select name="product_id" id="">
            <?php foreach ($products as $product): ?>
                <option value="<?=$product->id?>"><?=$product->name?></option>
            <?php endforeach; ?>
        </select>

        <select name="day_name" id="">
            <option value="Monday">Monday</option>
            <option value="Tuesday">Tuesday</option>
            <option value="Wednesday">Wednesday</option>
            <option value="Thursday">Thursday</option>
            <option value="Friday">Friday</option>
            <option value="Saturday">Saturday</option>
            <option value="Sunday">Sunday</option>
        </select>
        <input type="submit" value="Assign">
    </form>

    <table style="margin-top: 50px">
        <tr>
            <th>Product</th>
            <th>Available day</th>
        </tr>
        <?php foreach ($filters as $filtered): ?>
            <tr>
                <td><?=(new WC_Product($filtered->product_id))->name?></td>
                <td><?=$filtered->day_name?></td>
                <td><a href="<?=plugins_url('/product-filter/includes/scripts/delete_from_filtered.php').'?delete='.$filtered->id?>">Удалить</a></td>
            </tr>
        <?php endforeach; ?>
    </table>

</div>

